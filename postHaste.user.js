// ==UserScript==
// @name              Post Haste
// @author            rabel001
// @version           1.1.18.19
// @description       Post text to hastebin.com and get a return address to it.
// @namespace         https://gitlab.com/rabel001/userscripts/blob/master/postHaste.user.js
// @updateURL         https://glcdn.githack.com/rabel001/userscripts/raw/master/postHaste.user.js
// @downloadURL       https://glcdn.githack.com/rabel001/userscripts/raw/master/postHaste.user.js
// @include           *
// @noframes
// @grant             unsafeWindow
// @grant             GM_xmlhttpRequest
// ==/UserScript==

((win) => {
	"use strict";

	/*
	 *
	 * Hatebin POST documentation:
	 * https://github.com/seejohnrun/haste-server/wiki/POST-api
	 *
	 */
	win.postHaste = function(text) {
		return new Promise((resolve, reject) => {
			var postUrl = new URL("https://hastebin.com/documents");
			GM_xmlhttpRequest({
				method: "POST",
				url: "https://hastebin.com/documents",
				data: text,
				headers: {
					"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
				},
				onload: (response) => {
					if(response.readyState === 4 && response.status === 200) {
						var hastelink = postUrl.origin + "/" + JSON.parse(response.responseText).key;
						resolve(hastelink);
					}
					else if(response.readyState === 4 && response.status === 204) {
						reject(response)
					}
				},
				onerror: (ex) => {
					reject(ex);
				}
			});
		});
	}



})((typeof unsafeWindow !== "undefined") ? unsafeWindow : window);