((global, doc) => {
	//realm library for css and classes
	//version 1.00
	var realm = (elm) => {
		realm.getElement = doc.querySelector.bind(doc);
		var val = realm.getElement(elm);
		Object.assign(val, {
			getStyles: () => {
				var styles = {};
				for(var i = 0; i < val.style.length; i++) {
					styles[val.style[i]] = val.style[val.style[i]]
				}
				return styles;
			},
			css: (s, v) => {
				try {
					if(v === undefined) {
						var r = JSON.stringify(s);
						var replace = r.replace("{", "").replace("}", "");
						var splits = replace.split(",");
						var items = {};
						for(var m in splits) {
							var newsplits = splits[m].split(":");
							var split1 = newsplits[0].replace(/['"]/g, '');
							var split2 = newsplits[1].replace(/['"]/g, '');
							val.style[split1] = split2;
						}
						val = realm.getElement(elm);
						return val;
					}
					else {
						val.style[s] = v;
						val = realm.getElement(elm);
						return val;
					}
				}
				catch(ERR) {
					return ERR.message;
				}			
			},
			addClass: (c) => {
				realm.getElement(elm).classList.add(c);
				val = realm.getElement(elm);
				return val;
			},
			getClassList: () =>{
				return val.classList.value;
			},
			ready: (r) => {
				realm.getElement(elm).onload = r();
			}
		});
		return val;
	}
	var addStyleSheet = (str) => {
		value = str;
		var _css = doc.createElement('style');
		_css.type = 'text/css';
		var _cssTextNode = doc.createTextNode(str);
		_css.appendChild(_cssTextNode);
		_css.id = "_css";
		doc.head.appendChild(_css);
		return str;
	}
	realm.addStyleSheet = addStyleSheet;
	global.realm = realm;
	if(global.realm) {
		realm.ready = true;
		global.console.log('realm is loaded');
	}
})(window, document);