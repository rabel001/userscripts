// ==UserScript==
// @name            Wikiwand Cleanup
// @author          rabel001
// @homepage        https://rabel001.gitlab.io
// @version         2.1.14.19
// @icon            http://wikiwand.com/favicon.ico
// @description     Removes reference links, changes link color to black, and delete unsightly elements
// @namespace       https://gitlab.com/rabel001/userscripts/blob/master/wikiwandCleanup.user.js
// @updateURL       https://glcdn.githack.com/rabel001/userscripts/raw/master/wikiwandCleanup.user.js
// @downloadURL     https://glcdn.githack.com/rabel001/userscripts/raw/master/wikiwandCleanup.user.js
// @include         *wikiwand.com*
// @noframes
// @grant           none
// ==/UserScript==

;(($) => {
     window.checkjQuery = setInterval(() => {
          if(window.location.pathname !== "/") {
               if(typeof jQuery !== "undefined") {
                    let items = [
                         ".title_icon",
                         "#read_more",
                         ".title_wrapper h2",
                         ".wiki_alert",
                         ".anch-link"
                    ];
                    items.forEach((e) => {
                         $(e).remove();
                    });
                    $("a").css("color", "black");
                    document.querySelectorAll("nav")[0].remove();
                    $("#toc_wrapper").remove();
                    $("#content").width("100%");
                    clearInterval(window.checkjQuery);
                    window.checkjQuery = "Complete!";
               }
          }
     }, 1000);
})(jQuery.noConflict(true));