// ==UserScript==
// @name            superStackOverEx.user.js
// @author          rabel001
// @homepage        https://rabel001.gitlab.io
// @version         1.7.14.19
// @icon            https://cdn.sstatic.net/Sites/stackoverflow/img/favicon.ico
// @description     Open StackExchange\StackOverflow\Other links in a new tab
// @namespace       https://gitlab.com/rabel001/userscripts/blob/master/superStackOverEx.user.js
// @updateURL       https://glcdn.githack.com/rabel001/userscripts/raw/master/superStackOverEx.user.js
// @downloadURL     https://glcdn.githack.com/rabel001/userscripts/raw/master/superStackOverEx.user.js
// @include         *stackoverflow.com*
// @include         *stackexchange.com*
// @include         *stackoverflowbusiness.com*
// @include         *superuser.com*
// @include         *serverfault.com*
// @include         *askubuntu.com*
// @include         *stackapps.com*
// @noframes
// @grant           none
// ==/UserScript==

((global, doc) => {
     'use strict';

     const pages = [
          "stackoverflow.com",
          "stackexchange.com",
          "stackoverflowbusiness.com",
          "superuser.com",
          "serverfault.com",
          "askubuntu.com",
          "stackapps.com",
          "github.com"
     ];

     const icons = {
          'Ballotbox': '&#x2611;',
          'Circle': '&#9899;',
          'FeatherArrow': '&#10163;',
          'Flag': '&#9873;',
          'HeavyAsterisk': '&#10033;',
          'HeavyCheckMark': '&#x2714;',
          'HeavyCheckBox': '&#9989;',
          'HeavyCrossMark': '&#10060;',
          'HeavyDashedArrow': '&#10144;',
          'HeavyGreekCross': '&#10010;',
          'HeavyHeart': '&#10084;',
          'HeavyX': '&#10006;',
          'RightArrowHead': '&#10148;',
          'Shogi': '&#9751;',
          'Square': '&#x25A3;',
          'Star4': '&#10022;',
          'Star5': '&#9733;',
          'Star6': '&#10038;',
          'Star8': '&#10039;',
          'Star12': '&#10041;',
          'StarCircled': '&#10026;'
     };

     const setBlank = (elem, page) => {
          //function setBlank(elem) {
          elem.target = "_blank";
          switch (page) {
               case pages.stackoverflow:
                    if (!elem.classList.contains("comment-user")) {
                         try {
                              elem.style.fontSize = '15px';
                              elem.style.color = 'red';
                              let code = elem.getElementsByTagName('code').item(0);
                              let nStyle = 'font-size: 15px; font-family: Arial,"Helvetica Neue",Helvetica,sans-serif; line-height: 1.26666667;';
                              let newstyle = (!!code.getAttribute('style')) ? code.getAttribute('style') + '' + nStyle : nStyle;
                              code.setAttribute('style', newstyle);
                         }
                         catch (e) { }
                    }
                    break;
          }
     };

     const setBallotbox = (elem, page) => {
          if (!elem.getAttribute("found")) {
               let span = doc.createElement('span');
               span.innerHTML = icons.RightArrowHead;
               span.id = "ascii-ballotbox";
               span.style.color = "green";
               span.style.fontSize = 'large';
               if (!elem.classList.contains("comment-user")) {
                    try {
                         //elem.innerHTML = "&nbsp;" + elem.innerHTML;
                         //elem.insertAdjacentElement('afterBegin', span);
                         elem.innerHTML = `${icons.RightArrowHead}&nbsp;${elem.innerHTML}`;
                         elem.setAttribute("found", true);
                    }
                    catch (err) {
                         //elem[0].innerHTML = "&nbsp;" + elem[0].innerHTML;
                         //elem[0].insertAdjacentElement('afterBegin', span);
                         elem[0].innerHTML = `${icons.RightArrowHead}&nbsp;${elem[0].innerHTML}`;
                         elem[0].setAttribute("found", true);
                    }
               }
          }
     };

     const setSmallRedWithArrowAndBlank = (elem) => {
          elem.innerHTML = icons.RightArrowHead + "&nbsp;" + elem.innerHTML;
          elem.style.color = "red";
          elem.target = "_blank";
     }

     const recursiveElements = (elem, page) => {
          if (elem.tagName == "A") {
               setBlank(elem, page);
               setBallotbox(elem, page);
          }
          else {
               [...elem.children].forEach((child) => {
                    recursiveElements(child, page);
               });
          }
     };

     const setBlanks = () => {
          doc.querySelectorAll(".post-text, .comment-copy, .comment-body, #raw-url").forEach((a) => {
               if (a.id === "raw-url") {
                    setSmallRedWithArrowAndBlank(a);
               }
               if (/<a/.test(a.outerHTML)) {
                    a.querySelectorAll('a').forEach((b) => {
                         setBlank(b, pages.stackoverflow);
                         setBallotbox(b, pages.stackoverflow);
                    });
               }
          });
          document.querySelectorAll(".markdown-body.entry-content.p-5 a").forEach((a) => {
               if (!a.classList.contains("anchor") && a.childElementCount === 0) {
                    setBlank(a, pages.stackoverflow);
                    setBallotbox(a, pages.stackoverflow);
               }
          });
     };

     let splitHostName = global.location.hostname.split('.');
     let host = splitHostName[splitHostName.length - 2];
     let root = splitHostName[splitHostName.length - 1];
     let hostname = `${host}.${root}`;
     if (pages.includes(hostname)) {
          setBlanks();
     }
})(window, document);