// ==UserScript==
// @name                PostImage.Org
// @author              rabel001
// @version             1.7.21.2019
// @description         Post image to postimage.org
// @namespace           https://gitlab.com/rabel001/userscripts/raw/master/postImage.org.user.js
// @unpdateURL          https://gitlab.com/rabel001/userscripts/raw/master/postImage.org.user.js
// @downloadURL         https://gitlab.com/rabel001/userscripts/raw/master/postImage.org.user.js
// @connect             postimages.org
// @include             *
// @noframes
// @grant               GM_xmlhttpRequest
// @grant               GM_addStyle
// @grant               GM_openInTab
// ==/UserScript==

if(!/#post-image$/.test(location.href)) {
     return;
}

(() => {
     document.head.querySelectorAll("*").forEach(e => {
          if(e.tagName === "LINK" || e.tagName === "STYLE") {
               e.remove();
          }
     });
     document.body.style.backgroundColor = "white";
     document.body.innerHTML = '<h1 style="margin: 10px; text-align: center;">Post Image</h1><div style="width: 100%; height: 1px; margin-top: 10px; margin-bottom: 10px; background-color: black;"></div>';
     document.body.innerHTML += '<div style="text-align: center; width: 70%; margin: auto;"><label>Enter image URL:<input style="margin-left: 10px;" type="text" name="text" id="postimgtxt"><input type="submit" name="submit" value="Submit" id="postimgbtn"></label></div>';
     function webRequest() {
          return new Promise((resolve, reject) => {
               GM_xmlhttpRequest({
                    method: "POST",
                    url: arguments[0],
                    data: arguments[1],
                    responseType: "json",
                    headers: {
                         "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
                         "X-Requested-With": "XMLHttpRequest"
                    },
                    onload: (response) => {
                         if(response.readyState === 4 && response.status === 200) {
                              resolve(response);
                         }
                         else {
                              reject(response.responseText);
                         }
                    },
                    onerror: (ex) => {
                         reject(ex);
                    }
               });
          });
     }

     function postImage(pic) {
          //var pic = prompt("Enter the image URL.");
          if(pic) {
               function rand_string(e) {
                    for(var t = "", i = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", n = 0; n < e; n++)
                         t += i.charAt(Math.floor(Math.random() * i.length));
                    return t;
               }
               var upload_session = rand_string(32);
               var udate = new Date();
               var session_upload = udate.getTime();
               var harr = JSON.stringify([void 0 !== screen.colorDepth ? screen.colorDepth : "", void 0 !== screen.width ? screen.width : "", void 0 !== screen.height ? screen.height : "", void 0 !== navigator.cookieEnabled ? "true" : "", void 0 !== navigator.systemLanguage ? navigator.systemLanguage : "", void 0 !== navigator.userLanguage ? navigator.userLanguage : "", udate.toLocaleString() ? udate.toLocaleString() : ""]);
               //var pic = "https://my.bincdn.com/bincdn.png";
               var postTo = "https://postimages.org/json/rr";
               var data = {
                    token: "61aa06d6116f7331ad7b2ba9c7fb707ec9b182e8",
                    upload_session: upload_session,
                    url: pic,
                    numfiles: 1,
                    gallery: "",
                    ui: harr,
                    expire: "",
                    optsize: 0,
                    session_upload: session_upload
               }

               var params = Object.keys(data).filter(function (k) {
                    return (typeof data[k] !== 'undefined');
               }).map(function (k) {
                    return encodeURIComponent(k) + '=' + encodeURIComponent(data[k]);
               }).join('&');
               webRequest(postTo, params).then((results) => {
                    console.log(results.response);
                    document.body.innerHTML += `<br><div style="text-align: center;">Image link: <a href="${results.response.url}" target="_blank">${results.response.url}</a></div>`;
                    GM_openInTab(results.response.url, {
                         active: true
                    });
               }).catch((ex) => {
                    alert("Error! Check console for logs.", "Error!");
                    console.log(ex);
               });
          }
     }
     document.querySelector("#postimgbtn").onclick = function () {
          postImage(document.querySelector("#postimgtxt").value);
     }
})();