// ==UserScript==
// @name            Google Video Grid
// @author          rabel001
// @homepage        https://rabel001.gitlab.io
// @version         2.9.20.2020
// @icon            http://google.com/favicon.ico
// @description     Change Google Video Page to Grid using PureCSS
// @namespace       https://gitlab.com/rabel001/userscripts/blob/master/googlevideo.grid.js
// @updateURL       https://glcdn.githack.com/rabel001/userscripts/raw/master/googlevideo.grid.js
// @downloadURL     https://glcdn.githack.com/rabel001/userscripts/raw/master/googlevideo.grid.js
// @include         *google.com/search*&tbm=vid&*
// @noframes
// @grant           none
// @run-at          ocument-end
// ==/UserScript==

(() => {
     "use strict";

     var pure = document.createElement("link");
     pure.href = "//unpkg.com/purecss@1.0.1/build/pure-min.css";
     pure.rel = "stylesheet";
     document.head.appendChild(pure);
     var grid = document.createElement("link");
     grid.href = "//unpkg.com/purecss@1.0.1/build/grids-min.css";
     grid.rel = "stylesheet";
     document.head.appendChild(grid);

     document.querySelector('#center_col').style = "width: 1350px; margin-left: 50px;";
     document.querySelector(".g").parentElement.classList.add("pure-g");
     //document.querySelector(".srg").classList.add("pure-g");
     document.querySelectorAll(".g").forEach(e => {
          e.classList.add("pure-u-1-2");
     });

     document.querySelectorAll('.s').forEach((e) => {
          if(e.querySelector("img") !== null) {
               e.removeAttribute('onload');
               e.children[1].style = "margin: 0px; float: left;";
               e.children[0].style = "height: 162px; background-color: black;";
               e.children[0].children[0].style = "height: 100%; width: 100%; margin: auto;";
               let a = e.querySelector('a');
               a.style.height = "100%";
               a.style.width = "100%";
               a.removeAttribute('onmousedown');
               a.removeAttribute("ping");
               a.target = "_blank";
               let gimg = e.querySelector('g-img');
               gimg.style = "height: 162px; width: 100%; overflow: hidden;";
               let img = e.querySelector('img');
               if(img.id !== "blank") {
                    img.style = "top: 0px; width: 250px; height: 162px; margin: auto;";
               }
          } else {
               let newhref = e.previousElementSibling.querySelector('a').href;
               let div1 = document.createElement('div');
               div1.style = "height: 162px; background-color: black;";
               let div2 = document.createElement('div');
               div2.style = "height: 100%; width: 100%; margin: auto;";
               let a = document.createElement('a');
               a.target = "_blank";
               a.href = newhref;
               let gimg = document.createElement('g-img');
               gimg.style = "position: relative; height: 162px; width: 100%; overflow: hidden; background-color: black;";
               let img = document.createElement('img');
               img.src = "https://upload.wikimedia.org/wikipedia/commons/f/fc/No_picture_available.png";
               img.style = "position: relative; top: 0; left: calc(50% - 75px); width: 150px; height: 162px; margin: auto; background-color: black;";
               img.id = "blank";
               gimg.appendChild(img);
               a.appendChild(gimg);
               div2.appendChild(a);
               div1.appendChild(div2);
               e.insertAdjacentElement('afterbegin', div1);
          }
          e.parentElement.parentElement.style.width = "49%";
          e.parentElement.parentElement.style.margin = "1px";
          e.firstElementChild.firstElementChild.style.background = "black";
     });
})();