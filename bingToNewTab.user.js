// ==UserScript==
// @name            Bing Links to New Tab
// @author          rabel001
// @homepage        https://rabel001.gitlab.io
// @version         4.7.12.19
// @icon            http://bing.com/favicon.ico
// @description     Replaces the intermediary bing page with the direct link to the videos and searches.
// @namespace       https://gitlab.com/rabel001/userscripts/blob/master/bingToNewTab.user.js
// @updateURL       https://glcdn.githack.com/rabel001/userscripts/raw/master/bingToNewTab.user.js
// @downloadURL     https://glcdn.githack.com/rabel001/userscripts/raw/master/bingToNewTab.user.js
// @include         *bing.com/videos/search*
// @include         *bing.com/news/search*
// @include         *bing.com/search*
// @noframes
// @grant           none
// ==/UserScript==

((Doc, Path, MO) => {
     'use strict';

     if (!Doc && !Path && !MO) {
          console.log("Doc, Path, and MO do not exist.");
          return;
     }

     var videorolls = false; //true;

     /*==============================*/
     /* ---Hexadecimal characters--- */
     /*==============================*/
     const icons = {
          'BallotBox': '&#x2611;',
          'Circle': '&#9899;',
          'ExitBox': '&#10062;',
          'FeatherArrow': '&#10163;',
          'Flag': '&#9873;',
          'HeavyAsterisk': '&#10033;',
          'HeavyCheckMark': '&#x2714;',
          'HeavyCheckBox': '&#9989;',
          'HeavyCrossMark': '&#10060;',
          'HeavyDashedArrow': '&#10144;',
          'HeavyGreekCross': '&#10010;',
          'HeavyHeart': '&#10084;',
          'HeavyX': '&#10006;',
          'LargeX': '&#10060;',
          'RightArrowHead': '&#10148;',
          'Shogi': '&#9751;',
          'Square': '&#x25A3;',
          'Star4': '&#10022;',
          'Star5': '&#9733;',
          'Star6': '&#10038;',
          'Star8': '&#10039;',
          'Star12': '&#10041;',
          'StarCircled': '&#10026;'
     };

     /******************************************************** AD METHODS *********************************************************/
     const FILTER_ADS = (element) => {
          if (element.classList.contains('b_ad')) {
               element.remove();
          }
     }

     /**************************************************** VIDEO PAGE METHODS *****************************************************/
     const VIDEO_LINK_NOT_FOUND_YET = (element) => element.nextElementSibling !== null;
     const VIDEO_LINK_EDIT = (item) => {
          if (item.nextElementSibling !== null) {
               var purl;
               try {
                    var dataEventPayload = (item.querySelector('div[data-eventpayload') !== null) ? item.querySelector('div[data-eventpayload') : item.querySelector('.vrhdata');
                    if (dataEventPayload.className === 'vrhdata') {
                         purl = JSON.parse(dataEventPayload.getAttribute('vrhm')).murl;
                    }
                    //var dataEventpayload = item.querySelector('div[data-eventpayload').getAttribute('data-eventpayload');
                    else {
                         purl = JSON.parse(dataEventPayload.getAttribute('data-eventpayload')).purl;
                    }
                    //var purl = JSON.parse(dataEventpayload).purl;
                    if (/youtube/.test(purl)) {
                         if (videorolls) {
                              purl = 'http://videorolls.com/watch?v=' + purl.split('=')[1];
                         }
                    }
                    item.childNodes[0].childNodes[0].href = purl;
                    item.childNodes[0].childNodes[0].target = "_blank";

                    //var tl = item.querySelector('.tl');
                    var tl = item.querySelectorAll('.mc_vtvc_title')[0];
                    var span = Doc.createElement('span');
                    span.innerHTML = icons.BallotBox + "&nbsp;";
                    //span.innerHTML = "&#x2611; ";
                    span.id = "square";
                    span.style.color = "red";
                    if (!tl.childNodes[0].id) {
                         tl.insertAdjacentElement('afterbegin', span);
                    }
               } catch (err) { }
          }
     };

     /*************************************************** SEARCH PAGE METHODS ******************************************************/
     const SEARCH_LINK_NOT_FOUND_YET = (a) => (!a.getAttribute("found"));
     const SEARCH_LINK_EDIT = (item) => {
          var span = Doc.createElement('span');
          span.innerHTML = icons.BallotBox + "&nbsp;";
          //span.innerHTML = " &#x2611; ";
          span.id = "square";
          span.style.color = "red";
          item.target = '_blank';
          item.setAttribute('found', true);
          if (Path === "/news/search") {
               item.insertAdjacentElement("afterbegin", span);
          } else {
               item.parentElement.insertAdjacentElement("afterbegin", span);
          }
          item.setAttribute('found', true);
     };

     /***************************************************** OBSERVE METHOD ********************************************************/
     var observer = new MO((mutations) => {
          mutations.forEach((mutation) => {
               if (mutation.addedNodes.length > 0) {
                    mutation.addedNodes.forEach((newnode) => {
                         if (newnode.childNodes.length > 0) {
                              if (newnode.querySelector('a[href*="cache.aspx"]')) {
                                   let cachedLink = newnode.querySelector('a[href*="cache.aspx"]');
                                   if (SEARCH_LINK_NOT_FOUND_YET(cachedLink)) {
                                        SEARCH_LINK_EDIT(cachedLink);
                                   }
                              }
                         }
                    });
               }
          });
          if (Path === "/videos/search") {
               var VIDEO_LINKS = [...Doc.querySelectorAll(".dg_u, .mc_fgvc_u")];
               //var VIDEO_LINKS = ([...Doc.querySelectorAll('.dg_u')].length !== 0) ? [...Doc.querySelectorAll('.dg_u')] : [...Doc.querySelectorAll('.mc_fgvc_u')];
               var VIDEO_LINK_NOT_DONE_YET = VIDEO_LINKS.filter(VIDEO_LINK_NOT_FOUND_YET);
               VIDEO_LINK_NOT_DONE_YET.filter(VIDEO_LINK_EDIT);
          }
          if (Path === "/search") {
               var SEARCH_LINKS = [...Doc.querySelectorAll('h2 a[h], .na_t, .va_title_mmftb, .deeplink_title, .b_rs a')]; //.b_fcv2 a
               var SEARCH_LINK_NOT_DONE_YET = SEARCH_LINKS.filter(SEARCH_LINK_NOT_FOUND_YET);
               SEARCH_LINK_NOT_DONE_YET.filter(SEARCH_LINK_EDIT);
          }
          if (Path === "/news/search") {
               var NEWS_LINKS = [...Doc.querySelectorAll(".caption a.title")];
               var NEWS_LINKS_NOT_DONE_YET = NEWS_LINKS.filter(SEARCH_LINK_NOT_FOUND_YET);
               NEWS_LINKS_NOT_DONE_YET.filter(SEARCH_LINK_EDIT);
          }

          /*=========================*/
          /*  Remove advertisements  */
          /*=========================*/
          var SEARCH_LIST = [...Doc.querySelectorAll('#b_results li')];
          SEARCH_LIST.filter(FILTER_ADS);
     });
     var targetNode = Doc.body;
     var observerConfig = {
          childList: true,
          subtree: true
     };
     observer.observe(targetNode, observerConfig);

})(
     document,
     location.pathname.toLowerCase(),
     window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver
);