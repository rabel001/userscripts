// ==UserScript==
// @name            Inject Script
// @author          rabel001
// @homepage        https://rabel001.gitlab.io
// @version         3.7.21.19
// @description     Add scripts to current page using injectScript("script.js");
// @description     Or injectScript('console.log("Testing!")');
// @description     Or injectScript.available_Scripts.<scriptname>.inject();
// @description     Or injectScript(injectScript.available_Scripts.<scriptname>);
// @namespace       https://gitlab.com/rabel001/userscripts/blob/master/injectScript.user.js
// @updateURL       https://glcdn.githack.com/rabel001/userscripts/raw/master/injectScript.user.js
// @downloadURL     https://glcdn.githack.com/rabel001/userscripts/raw/master/injectScript.user.js
// @include         *
// @noframes
// @grant           none
// ==/UserScript==

((global, doc) => {
     "use strict";

     function webRequest(url, json) {
          return new Promise((resolve) => {
               /*
               GM_xmlhttpRequest({
                    url: url,
                    method: "GET",
                    reponseType: "json",
                    onload: function(result) {
                         resolve(result.response);
                    }
               });

               var xhr = new XMLHttpRequest();
               xhr.open("GET", url);
               (json) ? xhr.responseType = "json" : null;
               xhr.onload = function() {
                    if(this.readyState === 4 && this.status === 200) {
                         resolve(this.response);
                    }
               }
               xhr.send();
               */
               fetch(url, {
                    method: "GET",
                    headers: {
                         "Content-Type": (json) ? "application/json" : "application/x-www-form-urlencoded"
                    },
                    mode: "no-cors"
               }).then(function (e) {
                    if(json) {
                         e.json().then(function (text) {
                              resolve(text);
                         });
                    }
                    else {
                         e.text().then(function (text) {
                              resolve(text);
                         });
                    }
               });
          });
     }
     let _injectScript = (s) => {
          var help;
          if(typeof s === "object") {
               if(!!s.script) {
                    help = s.help.page;
                    s = s.script;
               }
          }
          else if(!(/^(?:https?:\/\/|\/\/)[^$]+\.js$/.test(s))) {
               try {
                    var newScript = doc.createElement("script");
                    newScript.innerText = s;
                    newScript.classList.add("newScriptText");
                    //doc.head.appendChild(newScript);
                    doc.head.append(newScript);
               }
               catch (ex) {
                    console.log("injectScript Output:\n\t - Error: could not load script:", ex);
                    //console.log("injectScript Output:\n\t - Error: Script must be a url containing http(s).");
                    console.log(s);
               }
               return;
          }
          let promise = async function (nscript) {
               return new Promise((resolve, reject) => {
                    let continuation = (nscript) => {
                         try {
                              let newScript = doc.createElement("script");
                              newScript.type = "text/javascript";
                              newScript.className = "newScript";
                              newScript.src = nscript;
                              newScript.onerror = function (event) {
                                   reject(`injectScript output:\n\t-Error:\n\t\t- Script failed to load with error code "${event.type}".\n\t\t- Script Name: ${nscript}`);
                              };
                              newScript.onload = function (event) {
                                   resolve(`injectScript output:\n\t- Loaded: true\n\t- Script Element: ${newScript.outerHTML}`);
                              };
                              //doc.head.appendChild(newScript);
                              doc.head.append(newScript);
                         }
                         catch (err) {
                              let err0 = `injectScript Output:\n\t- Error loading ${nscript.substring(nscript.lastIndexOf("/") + 1, nscript.length)}\n\t- Error message: ${err.message}`;
                              reject(err0);
                         }
                    };
                    if(!/\.js$/i.test(nscript)) {
                         reject(`\t-Error:\n\t\t- File name: ${nscript}.\n\t\t- ERROR: File extension should by ".js".`);
                    }
                    else if(doc.getElementsByClassName("newScript").length !== 0) {
                         if(!!~[...document.getElementsByClassName("newScript")].reduce((acc, v) => ((acc.push(v.src)), acc), []).indexOf(nscript)) {
                              reject("\t-Error: Script is already loaded");
                         }
                         else {
                              continuation(nscript);
                         }
                    }
                    else {
                         continuation(nscript);
                    }
               });
          }
          return new Promise((resolve) => {
               promise(s).then((data) => {
                    console.log(data);
                    if(help !== undefined) {
                         console.log("\t- Script Help:", help);
                    }
                    resolve(true);
               }).catch((error) => {
                    console.log(error);
                    resolve(false);
               });
          });

     };
     _injectScript.injectString = (text) => {
          let promise = (ntext) => {
               return new Promise((resolve, reject) => {
                    let continuation = (ntext) => {
                         try {
                              let newScript = doc.createElement("script");
                              newScript.innerText = ntext;
                              newScript.className = "newScript";
                              //doc.head.appendChild(newScript);
                              doc.head.append(newScript);
                              resolve("injectScript Output:\n\t- Loaded: true");
                         }
                         catch (ex) {
                              let err0 = `injectScript Output:\n\t- Error loading script\n\t- Error message: ${ex.message}`;
                              reject(err0);
                         }
                    };
                    if(doc.getElementsByClassName("newScript").length !== 0) {
                         if(!!~[...document.getElementsByClassName("newScript")].reduce((acc, v) => ((acc.push(v.textContent)), acc), []).indexOf(ntext)) {
                              reject("injectScript Output:\n\t- Error: Script is already loaded");
                         }
                         else {
                              continuation(ntext);
                         }
                    }
                    else {
                         continuation(ntext);
                    }
               });
          };
          promise(text).then((data) => {
               console.log(data);
          }).catch((error) => {
               console.log(error);
          });
     };
     _injectScript.loadLocationHref = () => {
          if(/\.js$/.test(location.href)) {
               _injectScript(location.href);
          }
          else {
               console.error('\tERROR: location.href does not end with a ".js" extension.');
          }
     }
     _injectScript.queryCDN = async (query) => {
          _injectScript.queryCDN.scripts = {};
          var url = `https://api.cdnjs.com/libraries?search=${query}`;
          webRequest(url, true).then((response) => {
               response.results.forEach((s) => {
                    s.name = s.name.replace(/-/g, "_");
                    _injectScript.queryCDN[s.name] = {
                         name: s.name,
                         script: s.latest,
                         inject: function () {
                              _injectScript(this.script);
                         },
                         getInfo: function () {
                              webRequest(`https://api.cdnjs.com/libraries/${this.name}`, true).then((response) => {
                                   console.log(response);
                                   //if(response.homepage) {
                                   _injectScript.queryCDN[s.name].info = response;
                                   //console.log('%cThis text is styled!', 'color: #86CC00; background-color: blue; font-size: 20px; padding: 3px;')
                                   console.log(`%cInformation was added to%c"injectScript.queryCDN.${s.name}.info".`, 'color: black; font-size: 20px; padding: 3px;', 'color: rgb(60, 179, 113); font-size: 20px; padding: 3px;');
                                   //}
                                   //if(response.repository) {
                                   //_injectScript.queryCDN.scripts[s.name].repository = response.repository.url;
                                   //}
                              });
                         },
                         getHomepage: function () {
                              webRequest(`https://api.cdnjs.com/libraries/${this.name}`, true).then((response) => {
                                   window.open(response.homepage, "_blank");
                              });
                         },
                         getRepository: function () {
                              webRequest(`https://api.cdnjs.com/libraries/${this.name}`, true).then((response) => {
                                   if(response.repository) {
                                        var repo = response.repository.url.split(":")[1];
                                        window.open(repo, "_blank");
                                   }
                              });
                         }
                    }
                    console.log(s);
               });
          });
     }
     _injectScript.queryCDN.url = 'https://api.cdnjs.com/libraries?search=';
     _injectScript.log = function () {
          return new Promise((resolve) => {
               var arr = arguments[0];
               var text = "";
               arr.forEach(t => {
                    text += t[0];
               });
               var logitems = [];
               logitems.push(text);
               arr.forEach(t => {
                    logitems.push(t[1]);
               });
               var log = console.log.bind(console);
               log.apply(console, logitems);
               resolve(true);
          });
     }
     _injectScript.queryCDN.getHelp = function () {
          var text = [];
          text.push([`%c1. Use the command `, "font: 18px roboto; color: black;"]);
          text.push([`%c"injectScript.queryCDN('any_word_here')" `, "font: 18px roboto; color: green;"]);
          text.push([`%cto query `, "font: 18px roboto; color: black"]);
          text.push([`%c"https//api.cdnjs.com/libraries?search=[query]" `, "font: 18px roboto; color: green"]);
          text.push([`%cfor a list of available script.\n\t\tA. `, "font: 18px roboto; color: black"]);
          text.push([`%c"any_word_here" `, "font: 18px roboto; color: green;"]);
          text.push([`%cis the name of a script you might be able to find.\n\t\tB. Maybe you want something to convert markdown to to HTML. Try `, "font: 18px roboto; color: black"]);
          text.push([`%c"injectScript.queryCDN('markdown')"`, "font: 18px roboto; color: green;"]);
          text.push([`%c.\n`, "font: 18px roboto; color: black"]);
          text.push([`%c2. Once you've made a query, script names and urls are added to `, "font: 18px roboto; color: black;"]);
          text.push([`%c"injectScript.queryCDN"`, "font: 18px roboto; color: green;"]);
          text.push([`%c.\n`, "font: 18px roboto; color: black;"]);
          text.push([`\t\t%cA. With the `, "font: 18px roboto; color: black;"]);
          text.push([`%c"injectScript.queryCDN('markdown')" `, "font: 18px roboto; color: green;"]);
          text.push([`%cexample, you can dot tab for the names.`, "font: 18px roboto; color: black;"]);
          _injectScript.log(text);
     };
     _injectScript.available_Scripts = {
          html5shiv: {
               name: "HTML5 Shiv",
               script: "https://raw.githack.com/aFarkas/html5shiv/master/dist/html5shiv.js",
               version: "3.7.3",
               help: {
                    page: "https://github.com/aFarkas/html5shiv"
               }
          },
          jquery1113: {
               name: "jQuery",
               script: "https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.js",
               version: "1.11.3",
               help: {
                    page: "https://jquery.com"
               }
          },
          jquery224: {
               name: "jQuery",
               script: "https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.js",
               version: "2.2.4",
               help: {
                    page: "https://jquery.com"
               }
          },
          jquery300: {
               name: "jQuery",
               script: "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.js",
               version: "3.0.0",
               help: {
                    page: "https://jquery.com"
               }
          },
          jqueryLatest: {
               name: "jQuery Latest",
               script: "https://code.jquery.com/jquery-latest.js",
               version: "Latest",
               help: {
                    page: "https://jquery.com"
               }
          },
          underscore: {
               name: "Underscore",
               script: "https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js",
               version: "1.8.3",
               help: {
                    page: "http://underscorejs.org"
               }
          },
          angular: {
               name: "Angular",
               script: "https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular.min.js",
               version: "1.6.5",
               help: {
                    page: "https://github.com/angular/angular.js"
               }
          },
          platform: {
               name: "Platform",
               script: "https://raw.githack.com/bestiejs/platform.js/master/platform.js",
               version: "1.3.5",
               help: {
                    page: "https://github.com/bestiejs/platform.js"
               }
          },
          bowser: {
               name: "Bowser",
               script: "https://raw.githack.com/lancedikson/bowser/master/src/bowser.js",
               version: "2.0",
               help: {
                    page: "https://github.com/ded/bowser"
               }
          },
          arrive: {
               name: "Arrive",
               script: "https://raw.githack.com/uzairfarooq/arrive/master/src/arrive.js",
               version: "2.4.1",
               help: {
                    page: "https://github.com/uzairfarooq/arrive"
               }
          },
          zest: {
               name: "Zest",
               script: "https://raw.githack.com/chjj/zest/master/lib/zest.js",
               version: "2012",
               help: {
                    page: "https://github.com/chjj/zest"
               }
          },
          HTML: {
               name: "HTML",
               script: "https://raw.githack.com/nbubna/HTML/master/dist/HTML.js",
               version: "0.12.1",
               help: {
                    page: "http://nbubna.github.io/HTML"
               }
          },
          head: {
               name: "Head",
               script: "https://cdnjs.cloudflare.com/ajax/libs/headjs/1.0.3/head.min.js",
               version: "1.0.3",
               help: {
                    page: "http://headjs.com"
               }
          },
          tags: {
               name: "Tag",
               script: "https://raw.githack.com/a-tarasyuk/tag/master/src/tag.js",
               version: "October 2018",
               help: {
                    page: "https://github.com/a-tarasyuk/tag"
               }
          },
          sugar: {
               name: "Sugar",
               script: "https://raw.githack.com/andrewplummer/Sugar/2.0.4/dist/sugar.js",
               version: "2.0.4",
               help: {
                    page: "https://sugarjs.com"
               }
          },
          requirejs: {
               name: "Require",
               script: "https://cdnjs.cloudflare.com/ajax/libs/require.js/2.3.5/require.min.js",
               version: "2.3.5",
               help: {
                    page: "https://requirejs.org"
               }
          },
          detectBrowser: {
               name: "Detect",
               script: "https://raw.githack.com/darcyclarke/Detect.js/master/detect.js",
               version: "2.2.2",
               help: {
                    page: "https://github.com/darcyclarke/Detect.js"
               }
          },
          is: {
               name: "Is",
               script: "https://raw.githack.com/arasatasaygin/is.js/master/is.js",
               version: "0.9.0",
               help: {
                    page: "http://is.js.org"
               }
          },
          xregexp: {
               name: "xregexp",
               script: "https://unpkg.com/xregexp@4.2.0/xregexp-all.js",
               version: "4.2.0",
               help: {
                    page: "http://xregexp.com"
               }
          },
          respond: {
               name: "Respond",
               script: "https://oss.maxcdn.com/respond/1.4.2/respond.min.js",
               version: "1.4.2",
               help: {
                    page: "https://github.com/scottjehl/Respond"
               }
          },
          dataTables: {
               name: "DataTables",
               script: "https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js",
               version: "1.10.16",
               help: {
                    page: "https://www.datatables.net"
               }
          },
          remotestorage: {
               name: "Remote Storage",
               script: "https://raw.githack.com/remotestorage/remotestorage.js/master/release/remotestorage.js",
               version: "1.3.0",
               help: {
                    page: "https://github.com/remotestorage/remotestorage.js"
               }
          },
          remotestorage_widget: {
               name: "Remote Storage Widget",
               script: "https://raw.githack.com/remotestorage/remotestorage-widget/master/build/widget.js",
               version: "1.3.0",
               help: {
                    page: "https://github.com/remotestorage/remotestorage-widget"
               }
          },
          displacejs: {
               name: "DisplaceJS",
               script: "https://raw.githack.com/catc/displace/master/dist/displace.js",
               version: "1.2.4",
               help: {
                    page: "https://catc.github.io/displace/"
               }
          },
          jsencrypt: {
               name: "Jsencrypt",
               script: "https://raw.githack.com/travist/jsencrypt/master/bin/jsencrypt.js",
               version: "May 2018",
               help: {
                    page: "https://github.com/travist/jsencrypt"
               }
          },
          cookies: {
               name: "Cookies",
               script: "https://raw.githack.com/florian/cookie.js/deaad9737e61c82a8b6e50053620cc426d645662/cookie.js",
               version: "Nov 8, 2016",
               help: {
                    page: "https://github.com/florian/cookie.js"
               }
          },
          webtorrent: {
               name: "webtorrent",
               script: "https://cdn.jsdelivr.net/webtorrent/latest/webtorrent.min.js",
               version: "latest",
               help: {
                    page: "https://webtorrent.io/intro"
               }
          },
          draggabilly: {
               name: "draggabilly",
               scripts: "https://unpkg.com/draggabilly@2/dist/draggabilly.pkgd.min.js",
               version: "2",
               help: {
                    page: "https://github.com/desandro/draggabilly"
               }
          },
          cryptico: {
               name: "Cryptico",
               script: "https://raw.githack.com/wwwtyro/cryptico/master/cryptico.js",
               version: "2016",
               help: {
                    page: "https://github.com/wwwtyro/cryptico",
                    example: '\nvar PassPhrase = "ultra-strong-password"\n' +
                         'var Bits = 2048;\n' +
                         'var MyRSAkey = cryptico.generateRSAKey(PassPhrase, Bits);\n' +
                         'var MyPublicKeyString = cryptico.publicKeyString(MyRSAkey);\n' +
                         'var EncryptedResult = cryptico.encrypt("Whatever PlainText", MyPublicKeyString);\n' +
                         'var DecryptedResult = cryptico.decrypt(EncryptedResult.cipher, MyRSAkey);'
               }
          },
          Gibberish_AES: {
               name: "Gibberish AES",
               script: "https://raw.githack.com/mdp/gibberish-aes/master/dist/gibberish-aes-1.0.0.js",
               version: "1.0.0",
               help: {
                    page: "https://github.com/mdp/gibberish-aes",
                    example: '\nvar enc = GibberishAES.enc("This sentence is super secret", "ultra-strong-password");\n' +
                         'var dec = GibberishAES.dec(enc, "ultra-strong-password");\n'
               }
          },
          easyXDM: {
               name: "easyXDM",
               script: "https://cdnjs.cloudflare.com/ajax/libs/easyXDM/2.4.20/easyXDM.min.js",
               version: "2.4.20",
               help: {
                    page: "https://github.com/oyvindkinsey/easyXDM",
                    example: "var socket = new easyXDM.Socket({\n" +
                         "     onMessage:function(message, origin) {\n" +
                         "          //do something with message\n" +
                         "     }\n" +
                         "});"
               }
          },
          javascriptObfuscator: {
               name: "Javascript Obfuscator",
               script: "https://cdn.jsdelivr.net/npm/javascript-obfuscator/dist/index.browser.js",
               version: "0.18.1",
               help: {
                    page: "https://obfuscator.io"
               }
          },
          slang: {
               name: "slang",
               script: "https://raw.githack.com/devongovett/slang/master/slang.min.js",
               version: "0.3.0",
               help: {
                    page: "https://github.com/devongovett/slang"
               }
          },
          Microjs: {
               name: "microjs",
               script: "https://microjs.com/data-min.js",
               version: "current",
               help: {
                    page: "http://microjs.com"
               },
               injectLibrary() {
                    _injectScript(_injectScript.available_Scripts.Microjs).then((w) => {
                         _injectScript.available_Scripts.Microjs.library = [...MicroJS.entries()].reduce((acc, [k, v]) => ((acc[v.name.replace(".", "").replace("-", "_")] = v), acc), {});
                         console.log("MicroJS library:", "injectScript.available_Scripts.Microjs.library");
                         console.log("MicroJS library:", _injectScript.available_Scripts.Microjs.library);
                    });
               },
               inject2() {
                    webRequest("http://microjs.com/data-min.js").then((text) => {
                         window.eval(text);
                         _injectScript.available_Scripts.Microjs.injectLibrary();
                    });
               }
          }
     };
     //var list;
     for(var key in _injectScript.available_Scripts) {
          if(_injectScript.available_Scripts.hasOwnProperty(key)) {
               var s = _injectScript.available_Scripts[key];
               s.inject = function () {
                    _injectScript(this);
               };
               s.help.open = function () {
                    window.open(this.page, "_blank");
               };
               var list = _injectScript.available_Scripts.list;
               //let scriptfile = s.script.substr(s.script.lastIndexOf("/") + 1);
               var scriptfile = s.name + ", Version: " + s.version;
               _injectScript.available_Scripts.list = (list) ? list + `${scriptfile}\r\n` : `${scriptfile}\r\n`;
          }
     }
     global.injectScript = _injectScript;
})(window, document);