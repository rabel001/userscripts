// ==UserScript==
// @name            _Styleable
// @author          rabel001
// @homepage        https://gitlab.com/rabel001/userscripts
// @version         1.00
// @description     Add stylesheets to head using _Styleable.add('<stylesheet>.js') or __Styleable.add(_Styleable.available_StyleSheets.<stylesheet_name>)
// @namespace       https://gitlab.com/rabel001/userscripts/blob/master/_Styleable.js
// @updateURL       https://glcdn.githack.com/rabel001/userscripts/raw/master/_Styleable.js
// @downloadURL     https://glcdn.githack.com/rabel001/userscripts/raw/master/_Styleable.js
// @include         *
// @include         about:blank*
// @grant           none
// ==/UserScript==

((global, doc) => {
	var _styleable = {
		add: (stylesheet) => {
			var help, failed = false;
			if(typeof stylesheet == "object") {
				if(!!stylesheet.stylesheet) {
					help = stylesheet.help;
					stylesheet = stylesheet.stylesheet;
				}
			}
			var promise = async (stylesheet) => {
				var rejected = false;
				return new Promise((resolve, reject) => {
					var continuation = (stylesheet) => {
						var newStyleSheet = doc.createElement('link');
						newStyleSheet.rel = "stylesheet";
						newStyleSheet.href = stylesheet;
						newStyleSheet.className = 'newStyleSheet';
						newStyleSheet.onerror = function(event) {
							failed = event.type;
						}
						doc.head.appendChild(newStyleSheet);
						var i = 0;
						var cycle = setInterval(() => {
							setTimeout(() => {
								if(!!doc.getElementsByClassName('newStyleSheet') && !failed) {
									resolve('\n\t- StyleSheet loaded: true\n\t- StyleSheet element: ' + newStyleSheet.outerHTML);
									clearInterval(cycle);
								}
								else {
									reject('\n\tError:\n\t\t- StyleSheet loaded: false\n\t\t- Error event type: ' + failed + '\n\t\t- StyleSheet: ' + stylesheet);
									clearInterval(cycle);
								}
							}, 98);
							i++;
						}, 100);
						if(i === 25) {
							clearInterval(cycle);
							reject('\n\tError loading: ' + stylesheet);
						}
					}
					if(!/\.css$/i.test(stylesheet)) {
						reject(`\n\tError:\n\t\t- File name: ${stylesheet}.\n\t\t- ERROR: File extension should by ".css".`);
					}
					else if(doc.getElementsByClassName('newStyleSheet').length !== 0) {
						var sheets = Array.from(doc.getElementsByClassName('newStyleSheet'));
						var arr = [];
						sheets.forEach((e) => {
							arr.push(e.href);
						});
						if(!!~arr.indexOf(stylesheet)) {
							reject('\n\tError: StyleSheet already loaded');
						}
						else {
							continuation(stylesheet);
						}
					}
					else {
						continuation(stylesheet);
					}
				});
			}
			promise(stylesheet).then((data) => {
				console.log('_Styleable Output: ' + data);
				if(help !== undefined) {
					console.log('\t- StyleSheet help: ' + help);
				}
			}).catch((error) => {
				console.log('_Styleable Output: ' + error);
			});
		},
		available_StyleSheets: {
			animate: {
				stylesheet: 'https://rawgit.com/daneden/animate.css/master/animate.css',
				help: 'http://daneden.github.io/animate.css'
			},
			sanitize: {
				stylesheet: 'https://cdnjs.cloudflare.com/ajax/libs/10up-sanitize.css/4.1.0/sanitize.min.css',
				help: 'https://jonathantneal.github.io/sanitize.css'
			},
			reset: {
				stylesheet: 'https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css',
				help: 'http://meyerweb.com/eric/tools/css/reset/'
			},
			normalize: {
				stylesheet: 'https://necolas.github.io/normalize.css/8.0.0/normalize.css',
				help: 'https://necolas.github.io/normalize.css'
			},
			dataTables: {
				stylesheet: '//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css',
				help: 'https://www.datatables.net'
			}
		}
	}
	global._Styleable = _styleable;
})(window, document);