// ==UserScript==
// @name            HTTPS Everywhere
// @author          rabel001
// @homepage        https://gitlab.com/rabel001/userscripts
// @version         1.00
// @description     Checks for HTTPS and addes link
// @namespace	    https://gitlab.com/rabel001/userscripts/blob/master/switchToHttps.js
// @updateURL	    https://glcdn.githack.com/rabel001/userscripts/raw/master/switchToHttps.js
// @downloadURL     https://glcdn.githack.com/rabel001/userscripts/raw/master/switchToHttps.js
// @include         *
// @run-at          document-start
// @grant           GM_xmlhttpRequest
// ==/UserScript==

function warn(url, trust) {
	var css = ".warn:hover { cursor: pointer; }";
	var style = document.createElement('style');
	style.appendChild(document.createTextNode(css));
	style.id = "warncss";
	document.head.appendChild(style);
	var divwarn = document.createElement("div");
	divwarn.style.width = "340px";
	divwarn.style.height = "30px";
	divwarn.style.lineHeight = "30px";
	divwarn.style.backgroundColor = "red";
	divwarn.style.color = "white";
	divwarn.style.textAlign = "center";
	divwarn.style.margin = "auto";
	divwarn.style.float = "left";
	divwarn.style.fontFamily = "arial";
	divwarn.style.fontSize = "16px";
	divwarn.id = "warning";
	if(trust) {
		divwarn.classList.add('warn');
		divwarn.innerText = "Warning: Switch to HTTPS";
	}
	else {
		divwarn.innerText = "Warning: HTTPS Not Available";
	}
	divx = document.createElement("div");
	divx.style.height = "30px";
	divx.style.lineHeight = "30px";
	divx.style.backgroundColor = "red";
	divx.style.color = "white";
	divx.style.float = "left";
	divx.style.textAlign = "center";
	divx.style.margin = "auto";
	divx.style.fontFamily = "arial";
	divx.style.fontSize = "16px";
	divx.classList.add('warn');
	divx.innerText = "X";
	divx.id = "exitwarn";
	var container = document.createElement("div");
	container.style.backgroundColor = "red";
	container.style.width = "370px";
	//container.style.height = "30px";
	container.style.position = "fixed";
	container.style.top = "80px";
	container.style.right = "10px";
	container.style.zIndex = "1000000000000000000000000";
	container.style.border = "3px white double";
	container.id = "warncontainer";
	container.appendChild(divwarn);
	container.appendChild(divx);
	document.body.insertAdjacentElement('afterBegin', container);
	document.getElementById("exitwarn").addEventListener("click", function(event) {
		event.target.parentElement.remove();
	}, false);
	if(trust) {
		document.getElementById("warning").addEventListener("click", function(event) {
			location.href = url;
		}, false);
	}
}
if(location.protocol === "http:") {
	var newurl = "https:" + window.location.href.substring(window.location.protocol.length);
	GM_xmlhttpRequest({
		method: "GET",
		url: newurl,
		onload: function(response) {
			//warn(newurl, true);
			if(localStorage.getItem(location.hostname)) {
				if(localStorage.getItem(location.hostname) === newurl) {
					warn(newurl, true);
				}
				else {
					localStorage.setItem(location.hostname, newurl);
					location.href = newurl;
				}
			}
			else {
				localStorage.setItem(location.hostname, newurl);
				location.href = newurl;
			}
		},
		onerror: function(response) {
			console.log("Error reaching " + newurl);
			warn("", false);
		}
	});
}