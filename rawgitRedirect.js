// ==UserScript==
// @name            Git Raw Redirect
// @author          rabel001
// @homepage        https://gitlab.com/rabel001/userscripts
// @version         1.01
// @description     Redirect raw.githubusercontent.com to rawgit.com and raw gitlab.com to glcdn.githack.com
// @description     Updated: 1/14/2019
// @icon            https://rawcdn.githack.com/neoascetic/rawgithack/4558441/sushi.png
// @namespace       https://gitlab.com/rabel001/userscripts/blob/master/rawgitRedirect.js
// @updateURL       https://glcdn.githack.com/rabel001/userscripts/raw/master/rawgitRedirect.js
// @downloadURL     https://glcdn.githack.com/rabel001/userscripts/raw/master/rawgitRedirect.js
// @include         *raw.githubusercontent.com*
// @include         *gitlab.com/*/raw/*
// @noframes
// @grant           none
// ==/UserScript==

(() => {
	'use strict';

    if(!/(\.gif|\.jpeg|\.jpg|\.png|\.txt)/.test(location.href)) {
        //Content security needs "sandbox 'allow-scripts'" to run for github raw scripts.
        if(/raw\.githubusercontent\.com/.test(location.href)) {
            //location.href = location.href.replace('raw.githubusercontent.com', 'rawgit.com');
            //location.href = location.href.replace('raw.githubusercontent.com', 'gitcdn.link/repo');
            location.href = location.href.replace('raw.githubusercontent.com', 'raw.githack.com');
        }
        if(/gitlab\.com/.test(location.hostname) && /raw/.test(location.pathname)) {
            location.href = location.href.replace('gitlab.com', 'glcdn.githack.com');
        }
    }
})();