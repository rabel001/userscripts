// ==UserScript==
// @name             Search DOM
// @author           rabel001
// @version          3..7.21.2019
// @description      Search DOM utility full of tools for search and creating elements.
// @namespace        SearchDOM.3.7.21.2019
// @updateURL        https://gitlab.com/rabel001/userscripts/raw/master/SearchDom.user.js
// @downloadURL      https://gitlab.com/rabel001/userscripts/raw/master/SearchDom.user.js
// @connect          *
// @connect          self
// @include          *
// @noframes
// @grant            none
// @run-at           document-end
// ==/UserScript==

((global) => {
     "use strict";

     global.onbeforeunload = null;

     var sDom = {
          qsa: document.querySelectorAll.bind(document),
          allLinks: Array.from(document.links),
          getflashvars: () => decodeURIComponent(document.querySelector('param[name="flashvars"], param[name="FlashVars"]').value),
          specials: ["-", "[", "]", "/", "{", "}", "(", ")", "*", "+", "?", ".", "\\", "^", "$", "|"],
          setSearchRegex(value) {
               var regex = new RegExp(`[${sDom.specials.join("\\")}]`, "g");
               var escapedValue = value.replace(regex, "\\$&");
               return new RegExp(escapedValue);
          },
          sleep(time) {
               return new Promise((resolve) => setTimeout(resolve, time));
          },
          fixCssText(cssText) {
               return cssText.replace(/\s+/g, " ")
                    .replace(/(?:\s*)?\{/g, " {\n\t")
                    .replace(/\}(?:\s*)?/g, "\n}\n")
                    .replace(/;/g, ";\n\t")
                    .replace(/^\s*[\r\n]/gm, "")
                    .replace(/\t\s*/g, "\t");
          },
          downloadImages(domElements) {
               var splitNum = Math.floor(domElements.length / 10);
               var m = [];
               var i = 0;
               for(i; i < splitNum; i++) {
                    m.push(domElements.splice(i, 10));
               }
               var remaining = `${domElements.length / 10}`;
               if(/\./.test(remaining)) {
                    m.push(domElements.splice(i, parseInt(remaining.split(".")[1])));
               }
               m.forEach(async (arr) => {
                    arr.forEach((a) => {
                         var evt = new MouseEvent("click", {
                              "view": global,
                              "bubbles": true,
                              "cancelable": true
                         });
                         //a.dispatchEvent(evt);
                    });
                    await sDom.sleep(1000);
               });
          },
          createBlobLinks(elem) {
               var arr = [];
               document.querySelectorAll(`${elem} a img`).forEach((img, i) => {
                    webRequestWithOpts({
                         url: img.parentNode.href,
                         responseType: "blob",
                         referrer: true
                    }).then((r) => {
                         img.parentNode.href = URL.createObjectURL(r.response);
                         img.parentNode.download = `a0000${i}.jpg`;
                         img.parentNode.target = "_blank";
                         img.parentNode.classList.add("sDom-pic");
                         arr.push(img.parentNode);
                         if(i === (document.querySelectorAll(`${elem} a img`).length - 1)) {
                              console.log(arr);
                              sDom.downloadImages(arr);
                         }
                    });
               });
          },
          RegexList: {
               ///
               /// use this to exlude string that contain certain strings: (?:(?!\.jpg))
               ///
               videoSrc: /<(?:video|source)\s.*?src=(?:'|")(?<item>[^'">]+)(?:'|")/,
               iFrameSrc: /<iframe\s.*?src=(?:'|")(?<item>[^'">]+)(?:'|")/,
               embedflashvars: /<embed\s.*?flashvars=(?:'|")(?<item>[^'"]+)(?:'|")/,
               embedSrc: /<embed\s.*?src=(?:'|")(?<item>[^'"]+)(?:'|")/,
               imgSrc: /<img\s.*?src=(?:'|")(?<item>[^'">]+)(?:'|")/,
               scriptTex: /<script[^>]*>(?<item>[\S\s]*?)<\/script>/,
               scriptSrc: /<script\s.*?src=(?:"|')(?<item>[^"']+)(?:"|')/,
               //mp4String: /(?:"|')((https?:\/\/|\/\/)[^"']+\.mp4(?:(?!\.jpg))(?:[^"']+)?)(?:"|')/,
               mp4String: /"(?<item>[^"\s]+\.mp4(?:(?!\.jpg))[^"\s]*?)"/,
               ///["']((?:https?:\/\/|\/\/)(?:[^'"]+)?(?:(?!thumb))(?:[^"']+)?\.(?:mp4|flv|webm|mov|avi|mp3|3gp|wma|wav|mpg|mpeg|m4a|oga)(?:(?!\.jpg))[^"']+(?:(?!thmumb))[^'"]+)["']/,
               m3u8String: /(?:"|')(?<item>(https?:\/\/|\/\/)[^"']+\.m3u8(?:[^"']+)?)(?:"|')/,
               quotedText: /("(?<item>.*?)"|'(?<item2>.*?)')/,
               eightchan: /(?:(?<item>h..ps?:\/\/[^\s]+)\s|(?<item2>aHR0c.*?)[<\s]|(?<item3>\/r\/[\w\d]+))/,
               addOverlay(theArray) {
                    if(document.querySelector('#regexlist')) {
                         document.querySelector('#regexlist').remove();
                    }
                    //var cssText = `#_roverlay,#_ul{height:100%!important}#_ul,#_ul > li{list-style:decimal inside!important;}#_ul > li{padding:3px !important;}#_rexit,#_openLink,#_rtext{position:absolute!important}#_roverlay{position:fixed!important;display:none;width:100%!important;top:0!important;left:0!important;right:0!important;bottom:0!important;background-color:rgba(0,0,0,.5)!important;z-index:200000!important}#_rtext{top:50%!important;left:50%!important;height:80%!important;width:80%!important;z-index:1!important;font-size:14px!important;font-family:Arial!important;color:#000!important;background-color:#fff!important;transform:translate(-50%,-50%)!important;-ms-transform:translate(-50%,-50%)!important}#_rexit{top:5px!important;right:5px!important;z-index:20000000!important;font-size:25px!important;font-weight:700!important;color:#fff!important;cursor:pointer!important}#_ul{overflow:auto!important;text-align:left!important;margin-left:20px!important;padding:2px!important}#_openLink,._openLink{font-size:14px!important;font-family:Arial!important;line-height:14px!important;height:30px!important;padding:5px!important;background-color:#444!important;color:#fff!important}#_openLink{top:11px!important;right:30px!important;border:0!important;border-radius:3px!important}._openLink{margin:2px!important;border:0!important;border-radius:3px!important}.clickedlink,.finddomlink:link{text-decoration:underline!important;font-size:13px!important;}.finddomlink:link{color:#00f!important}.clickedlink,.finddomlink:visited{color:purple!important}`;
                    var cssText = `#_roverlay { height: 100% !important; }`;
                    cssText += `#_ul { height: calc(100% - 30px) !important; }`;
                    cssText += `#_ul, #_ul>li { list-style: decimal inside !important; }`;
                    cssText += `#_ul>li { padding: 3px !important; }`;
                    cssText += `#_rexit, #_openLink, #_rtext { position: absolute !important; }`;
                    cssText += `#_roverlay { position: fixed !important; display: none; width: 100% !important; top: 0 !important; left: 0 !important;`;
                    cssText += `right: 0 !important; bottom: 0 !important; background-color: rgba(0, 0, 0, .5) !important; z-index: 200000 !important; }`;
                    cssText += `#_rtext { top: 50% !important; left: 50% !important; height: 80% !important; width: 80% !important; z-index: 1 !important;`;
                    cssText += `font-size: 14px !important; font-family: Arial !important; color: #000 !important; background-color: #fff !important;`;
                    cssText += `transform: translate(-50%, -50%) !important; -ms-transform: translate(-50%, -50%) !important; text-shadow: none !important; }`;
                    cssText += `#_rexit { top: 5px !important; right: 5px !important; z-index: 20000000 !important; font-size: 25px !important;`;
                    cssText += `font-weight: 700 !important; color: #fff !important; cursor: pointer !important; }`;
                    cssText += `#_ul { overflow: auto !important; text-align: left !important; margin-left: 20px !important; padding: 2px !important; }`;
                    cssText += `#_openLink, ._openLink { font-size: 14px !important; font-family: Arial !important; line-height: 14px !important;`;
                    cssText += `height: 30px !important; padding: 5px !important; background-color: #444 !important; color: #fff !important; }`;
                    cssText += `#_openLink { top: 11px !important; right: 30px !important; border: 0 !important; border-radius: 3px !important; }`;
                    cssText += `._openLink { margin: 2px !important;  border: 0 !important; border-radius: 3px !important; }`;
                    cssText += `.clickedlink, .finddomlink:link { text-decoration: underline !important; font-size: 13px !important; }`;
                    cssText += `.finddomlink:link { color: #00f !important; }`;
                    cssText += `.clickedlink, .finddomlink:visited { color: purple !important}`;
                    cssText = sDom.fixCssText(cssText);
                    var cssTextNode = document.createTextNode(cssText);
                    var css = document.createElement('style');
                    css.appendChild(cssTextNode);
                    css.id = 'regexlist';
                    document.head.appendChild(css);
                    var _roverlay = document.createElement('div');
                    _roverlay.id = '_roverlay';
                    var _rdiv = document.createElement('div');
                    _rdiv.id = '_rtext';
                    _rdiv.name = 'text';
                    //if(imports.text == null) imports.text = 'Nothing found';
                    var ul = document.createElement('ul');
                    ul.id = '_ul';
                    theArray.forEach((val, i) => {
                         var li = document.createElement('li');
                         li.classList.add('_li');
                         var a = document.createElement('a');
                         a.id = '_a' + i;
                         a.innerText = val;
                         a.href = val;
                         a.target = '_blank';
                         a.classList.add('finddomlink');
                         a.onclick = function () {
                              this.classList.remove('finddomlink');
                              this.classList.add('clickedlink');
                         };
                         //li.appendChild(checkbox);
                         //li.appendChild(openLink);
                         li.appendChild(document.createTextNode(' '));
                         li.appendChild(a);
                         ul.appendChild(li);
                    });
                    _rdiv.appendChild(ul);
                    _roverlay.appendChild(_rdiv);
                    _roverlay.style.display = 'block';
                    var span = document.createElement('span');
                    span.id = '_rexit';
                    span.innerHTML = '&#x2717;';
                    span.addEventListener('click', () => {
                         document.getElementById('_roverlay').remove(); //.style.display = 'none';
                    });
                    _roverlay.appendChild(span);
                    _roverlay.onclick = (evt) => {
                         if(evt.target.id === '_roverlay') {
                              //if(evt.srcElement.tagName === 'DIV') {
                              document.getElementById('_rexit').click();
                         }
                    };
                    document.body.appendChild(_roverlay);
               },
               searchQuotes(item) {
                    var regex = sDom.setSearchRegex(item);
                    var quotedRegex = new RegExp(sDom.RegexList.quotedText, "gm");
                    var outerHTML = document.documentElement.outerHTML.match(quotedRegex);
                    sDom.RegexList.addOverlay([...outerHTML].reduce((a, v) => (((regex.test(v)) ? a.push(v.replace(/["']/g, "")) : null), a), []));
               },
               init() {
                    for(let key in sDom.RegexList) {
                         if(sDom.RegexList.hasOwnProperty(key)) {
                              sDom.RegexList[key].retrieve = function () {
                                   var items = [];
                                   var rex = new RegExp(sDom.RegexList[key], "gm");
                                   var m;
                                   var outer;
                                   if(key === "eightchan") {
                                        outer = document.documentElement.outerText;
                                   }
                                   else {
                                        outer = document.documentElement.outerHTML;
                                   }
                                   [...outer.matchAll(rex)].forEach((item) => {
                                        if(item.groups.item) {
                                             items.push(item.groups.item);
                                        }
                                        if(item.groups.item2) {
                                             items.push(item.groups.item2);
                                        }
                                        if(item.groups.item3) {
                                             items.push(item.groups.item3);
                                        }
                                   })
                                   /*
                                   outer.match(rex).forEach((m) => {
                                        if(key === "eightchan" && !items.includes(m)) {
                                             if(/aHR0c/.test(m)) {
                                                  items.push(atob(m));
                                             }
                                             else if(/^\/r\//.test(m)) {
                                                  items.push("https://volafile.org" + m);
                                             }
                                             else {
                                                  items.push(m);
                                             }
                                        }
                                        else if(/(mp4String|iFrameSrc|videoSrc)/.test(key)) {
                                             m = m.match(/"([^"]+)"/)[1];
                                             if(!items.includes(m)) {
                                                  items.push(m);
                                             }
                                        }
                                        else {
                                             if(!items.includes(m)) {
                                                  items.push(m);
                                             }
                                        }
                                   });
                                   */
                                   //while((m = outer.match(rex)) !== null) {
                                   //while((m = document.documentElement.outerHTML.match(rex)) !== null) {
                                   //     if(m.index === rex.lastIndex) {
                                   //          rex.lastIndex++;
                                   //     }
                                   //     m.forEach((match, groupIndex) => {
                                   //          if(groupIndex === 1) {
                                   //               if(match) {
                                   //                    items.push(match);
                                   //               }
                                   //          }
                                   //     });
                                   //}
                                   sDom.RegexList.addOverlay(items);
                                   return items;
                              }
                         }
                    }
                    sDom.RegexList.getIframes = () => {
                         var frames = [...document.querySelectorAll("iframe")].reduce((a, v) => ((a.push(v.src)), a), []);
                         sDom.RegexList.addOverlay(frames);
                         return frames;
                    }
                    sDom.RegexList.getVideos = () => {
                         var vids = [...document.querySelectorAll("video, source")].reduce((a, v) => (((v.src) ? a.push(v.src) : null), a), []);
                         sDom.RegexList.addOverlay(vids);
                         return vids;
                    }
               }
          },
          getValues(element, value) {
               return new Promise((resolve) => {
                    var items = [];
                    var allElements = [...element.getElementsByTagName('*')];
                    var regex = sDom.setSearchRegex(value);
                    var thisHTML;
                    if(element.nodeName === "#document") {
                         thisHTML = element.documentElement.outerHTML;
                    }
                    else {
                         thisHTML = element.innerHTML;
                    }
                    var m = thisHTML.match(/("(.*?)"|'(.*?)')/g);
                    if(m) {
                         var qtexts = m.reduce((a, v) => (((regex.test(v)) ? a.push(decodeURIComponent(v).replace(/["']/g, "")) : null), a), []);
                         qtexts.forEach((txt) => {
                              items.push({
                                   QuotedText: txt
                              });
                         });
                    }
                    allElements.forEach((element) => {
                         if(element.hasAttributes()) {
                              for(let prop in element.attributes) {
                                   if(element.attributes.hasOwnProperty(prop)) {
                                        var key = element.attributes[prop].name;
                                        var val = element.attributes[prop].value;
                                        if(regex.test(key) || regex.test(val)) {
                                             if(!items.includes(JSON.stringify({
                                                  [key]: val
                                             }))) {
                                                  items.push({
                                                       [key]: val
                                                  });
                                             }
                                        }
                                   }
                              }
                         }
                         if(regex.test(element.innerText)) {
                              if(!items.includes(JSON.stringify({
                                   [element.tagName]: element.innerText
                              }))) {
                                   items.push({
                                        [element.tagName]: element.innerText
                                   });
                              }
                         }
                         if(element.tagName === value.toUpperCase()) {
                              if(!items.includes({
                                   [element.tagName]: element.outerHTML
                              })) {
                                   items.push({
                                        [element.tagName]: element.outerHTML
                                   });
                              }
                         }
                    });
                    resolve(items);
               });
          },
          findValue(element, value, withRegexList) {
               var dashes = '--------------------------------------------------------------------';
               global.fvOptions = {
                    vidExtensions: /(\.webm|\.mkv|\.flv|\.mp4|\.f4v|\.mpg|\.mpeg|\.wav|\.wmv|\.avi|\.mov|\.ogg)/,
               };
               if(!!document.getElementById('_overlay')) {
                    document.getElementById('_overlay').remove();
               }
               sDom.getValues(element, value).then((imports) => {
                    console.log("[searchDom:]", imports);
                    //var cssText = `#_overlay,#_ul{height:100%!important}#_ul,._li{list-style:decimal inside!important;}.li{padding: 3px !important;}#_exit,#_openLink,#_text{position:absolute!important}#_overlay{position:fixed!important;display:none;width:100%!important;top:0!important;left:0!important;right:0!important;bottom:0!important;background-color:rgba(0,0,0,.5)!important;z-index:200000!important}#_text{top:50%!important;left:50%!important;height:80%!important;width:80%!important;z-index:1!important;font-size:14px!important;font-family:Arial!important;color:#000!important;background-color:#fff!important;transform:translate(-50%,-50%)!important;-ms-transform:translate(-50%,-50%)!important}#_exit{top:5px!important;right:5px!important;z-index:20000000!important;font-size:25px!important;font-weight:700!important;color:#fff!important;cursor:pointer!important}#_ul{overflow:auto!important;text-align:left!important;margin-left:20px!important;padding:2px!important}#_openLink,._openLink{font-size:14px!important;font-family:Arial!important;line-height:14px!important;height:30px!important;padding:5px!important;background-color:#444!important;color:#fff!important}#_openLink{top:11px!important;right:30px!important;border:0!important;border-radius:3px!important}._openLink{margin:2px!important;border:0!important;border-radius:3px!important}.clickedlink,.finddomlink:link{text-decoration:underline!important;font-size:13px!important}.finddomlink:link{color:#00f!important}.clickedlink,.finddomlink:visited{color:purple!important}`;
                    var cssText = `#_overlay, #_ul { height: 100% !important; }`;
                    cssText += `#_ul, ._li { list-style: decimal inside !important; }`;
                    cssText += `.li { padding: 3px !important; }`;
                    cssText += `#_exit, #_openLink, #_text { position: absolute !important; }`;
                    cssText += `#_overlay { position: fixed !important; display: none; width: 100% !important; top: 0 !important; left: 0 !important;`;
                    cssText += `right: 0 !important; bottom: 0 !important; background-color: rgba(0, 0, 0, .5) !important; z-index: 200000 !important; }`;
                    cssText += `#_text { top: 50% !important; left: 50% !important; height: 80% !important; width: 80% !important; z-index: 1 !important;`;
                    cssText += `font-size: 14px !important; font-family: Arial !important; color: #000 !important; background-color: #fff !important;`;
                    cssText += `transform: translate(-50%, -50%) !important; -ms-transform: translate(-50%, -50%) !important; }`;
                    cssText += `#_exit { top: 5px !important; right: 5px !important; z-index: 20000000 !important; font-size: 25px !important;`;
                    cssText += `font-weight: 700 !important; color: #fff !important; cursor: pointer !important; }`;
                    cssText += `#_ul { overflow: auto !important; text-align: left !important; margin-left: 20px !important; padding: 2px !important; }`;
                    cssText += `#_openLink, ._openLink { font-size: 14px !important; font-family: Arial !important; line-height: 14px !important;`;
                    cssText += `height: 30px !important; padding: 5px !important; background-color: #444 !important; color: #fff !important; }`;
                    cssText += `#_openLink { top: 11px !important; right: 30px !important;  border: 0 !important; border-radius: 3px !important; }`;
                    cssText += `._openLink { margin: 2px !important; border: 0 !important; border-radius: 3px !important; }`;
                    cssText += `.clickedlink, .finddomlink:link { text-decoration: underline !important; font-size: 13px !important; }`;
                    cssText += `.finddomlink:link { color: #00f !important; }`;
                    cssText += `.clickedlink, .finddomlink:visited { color: purple !important; }`;
                    cssText = sDom.fixCssText(cssText);
                    var cssTextNode = document.createTextNode(cssText);
                    var css = document.createElement('style');
                    css.appendChild(cssTextNode);
                    css.id = 'findValues';
                    document.head.appendChild(css);
                    var _overlay = document.createElement('div');
                    _overlay.id = '_overlay';
                    //var _textarea = document.createElement('textarea');
                    var _textarea = document.createElement('div');
                    _textarea.id = '_text';
                    _textarea.name = 'text';
                    //if(imports.text == null) imports.text = 'Nothing found';
                    var ul = document.createElement('ul');
                    ul.id = '_ul';
                    /*
                    ol.onclick = function(evt) {
                         if(/checkbox/.test(evt.target.id)) {
                             var allCheckboxes = document.querySelectorAll('#_text #_ol input[type="checkbox"]');
                             allCheckboxes.forEach((cbox) => {
                                 if(cbox.id !== evt.target.id) {
                                     cbox.checked = false;
                                 }
                             });
                         }
                     }
                     */
                    global.vals = imports;
                    imports.forEach((item) => {
                         for(let key in item) {
                              var val = item[key];
                              var li = document.createElement('li');
                              li.classList.add('_li');
                              var a = document.createElement('a');
                              a.id = '_a' + key;
                              a.innerText = val;
                              a.href = val;
                              a.target = '_blank';
                              a.classList.add('finddomlink');
                              a.onclick = function () {
                                   this.classList.remove('finddomlink');
                                   this.classList.add('clickedlink');
                              };
                              li.appendChild(document.createTextNode(' '));
                              li.appendChild(a);
                              ul.appendChild(li);
                         }
                    });
                    _textarea.appendChild(ul);
                    _overlay.appendChild(_textarea);
                    _overlay.style.display = 'block';
                    var span = document.createElement('span');
                    span.id = '_exit';
                    span.innerHTML = '&#x2717;';
                    span.addEventListener('click', () => {
                         document.getElementById('_overlay').style.display = 'none';
                    });
                    _overlay.appendChild(span);
                    _overlay.onclick = (evt) => {
                         if(evt.target.id === '_overlay') {
                              //if(evt.srcElement.tagName === 'DIV') {
                              document.getElementById('_exit').click();
                         }
                    };
                    document.body.appendChild(_overlay);
                    global.fvOptions = null;
               });
          },
          runFunction() {
               if(!document.querySelector("#_runFunction")) {
                    var cssText = `#_runFuncOverlay { position: fixed !important; display: none; width: 100% !important; top: 0 !important;`;
                    cssText += `left: 0 !important; right: 0 !important; bottom: 0 !important; background-color: rgba(0, 0, 0, .5) !important;`;
                    cssText += `z-index: 200000 !important; }`;
                    cssText += `#_runFuncContainer, #_exit { position: absolute !important; }`;
                    cssText += `#_runFuncContainer { top: 50% !important; left: 50% !important; height: 80% !important; width: 80% !important;`;
                    cssText += `z-index: 1 !important; font-size: 14px !important; font-family: Arial !important; color: #000 !important;`;
                    cssText += `transform: translate(-50%, -50%) !important; -ms-transform: translate(-50%, -50%) !important; opacity: 0.2 !important; }`;
                    cssText += `#_runFuncTextarea { height: calc(100% - 47px) !important; width: 100% !important; padding: 10px !important;`;
                    cssText += `margin: 0 !important; border: 2px solid black !important; opacity: 1 !important; font-family: 'Open Sans' !important;`;
                    cssText += `font-size: 14px !important; line-height: 1.42857143 !important; color: #333333 !important; }`;
                    cssText += `#_runFuncSubmit { float: right !important; margin-right: 1px !important; opacity: 1 !important; }`;
                    cssText += `#_exit { top: 5px !important; right: 5px !important; z-index: 20000000 !important; font-size: 25px !important;`;
                    cssText += `font-weight: 700 !important; color: #fff !important; cursor: pointer !important; }`;
                    cssText = sDom.fixCssText(cssText);
                    var cssTextNode = document.createTextNode(cssText);
                    var css = document.createElement('style');
                    css.appendChild(cssTextNode);
                    css.id = '_runFunction';
                    document.head.appendChild(css);
               }
               var _runFuncOverlay = document.createElement('div');
               _runFuncOverlay.id = '_runFuncOverlay';
               var _runFuncContainer = document.createElement("div");
               _runFuncContainer.id = "_runFuncContainer";
               var _runFuncTextarea = document.createElement('textarea');
               _runFuncTextarea.id = '_runFuncTextarea';
               _runFuncTextarea.name = 'tex_runFuncTextareat';
               _runFuncContainer.appendChild(_runFuncTextarea);
               _runFuncOverlay.style.display = 'block';
               var _submit = document.createElement("input");
               _submit.id = "_runFuncSubmit";
               _submit.type = "submit";
               _submit.text = "Execute";
               _runFuncContainer.appendChild(_submit);
               _submit.onclick = function (params) {
                    var textFunc = document.querySelector("#_runFuncTextarea");
                    var m = new Function("return function() {" + textFunc.value + "}")();
                    m();
                    //var m = new Function("return function() { console.log('a'); }")();
               }
               //_runFuncTextarea.appendChild(_submit);
               var span = document.createElement('span');
               span.id = '_exit';
               span.innerHTML = '&#x2717;';
               span.addEventListener('click', () => {
                    document.getElementById('_runFuncOverlay').style.display = 'none';
               });
               _runFuncOverlay.appendChild(_runFuncContainer);
               _runFuncOverlay.appendChild(span);
               _runFuncOverlay.onclick = (evt) => {
                    if(evt.target.id === '_runFuncOverlay') {
                         //if(evt.srcElement.tagName === 'DIV') {
                         document.getElementById('_exit').click();
                    }
               };
               document.body.appendChild(_runFuncOverlay);
          },
          getWindowVariables(doReturn) {
               function isNative() {
                    //Reference: https://stackoverflow.com/a/17246535
                    var newframe = document.createElement("iframe");
                    newframe.style.display = "none";
                    document.body.appendChild(newframe);
                    var currentWindow = Object.getOwnPropertyNames(global);
                    var nonNatives = currentWindow.filter((prop) => !newframe.contentWindow.hasOwnProperty(prop));
                    document.body.removeChild(newframe);
                    var [funcs, vars] = nonNatives.reduce(([f, v], n) => {
                         if(typeof (global[n]) === "function") {
                              f[n] = global[n];
                         } else {
                              v[n] = global[n];
                         }
                         return [f, v];
                    }, [{}, {}]);
                    return { funcs, vars };
               }

               var { funcs, vars } = isNative();
               console.log("Variable Count:", [...Object.entries(vars)].length);
               let eq = Array(60).join("=");
               let title = `%c | ${eq} Window Variables ${eq} | `;
               console.log(title, "color: white; background-color: darkgreen; font-weight: bold");
               [...Object.entries(vars)].forEach((v) => {
                    console.log(`%c | %c ${v[0]}:`, "color: white; background-color: darkgreen; font-weight: bold;", "color: green; font-weight: bold", v[1]);
               });
               console.log(title, "color: white; background-color: darkgreen; font-weight: bold");
               let title2 = `%c | ${eq} Window Functions ${eq} | `;
               console.log("Function Count:", [...Object.entries(funcs)].length);
               console.log(title2, "color: white; background-color: blue; font-weight: bold");
               [...Object.entries(funcs)].forEach((f) => {
                    console.log(`%c | %c Function: %c${f[0]}()`, "color: white; background-color: blue; font-weight: bold;", "color: blue; font-weight: bold", "color: black");
               });
               console.log(title2, "color: white; background-color: blue; font-weight: bold");
               if(doReturn) {
                    return {
                         Variables: vars,
                         Functions: funcs
                    };
               }
          },
          testLocation(regex) {
               //regex = new RegEx(regex);
               return regex.test(location.href);
          },
          HTML: function () {
               this.doc = document;
               this.qs = this.doc.querySelector.bind(this.doc);
               this.qsa = this.doc.querySelectorAll.bind(this.doc);
               this.all = Array.from(this.doc.all);
               this.head = this.doc.head;
               this.body = this.doc.body;
               this.allQuotedTxt = document.documentElement.outerHTML.match(/(?:"(.*?)"|'(.*?)')/mg);
               this.links = Array.from(this.doc.links);
               this.links.reduce((acc, cur) => (
                    ((cur.search) ? cur.searchParams = [...new URLSearchParams(cur.search).entries()].reduce((acc2, [k, v]) => ((acc2[k] = v), acc2), {}) : null)));
               //this.link.reduce((acc, href) => {
               //     (href.search) ? href.searchParams = [...new URLSearchParams(href.search).entries()].reduce((acc, [k, v]) => {
               //          href.searchParams[k] = v;
               //     }) : null;
               //});
               this.styleSheets = Array.from(this.doc.styleSheets);
               this.scripts = Array.from(this.doc.scripts);
               this.scripts.withSource = [];
               this.scripts.withText = [];
               Array.from(this.scripts).reduce((acc, v) => (((v.src) ? this.scripts.withSource.push(v.src) : null, (v.text) ? this.scripts.withText.push(v.text) : null), acc), {});
               this.elements = [...Array.from(this.doc.all).entries()].reduce((acc, [k, v]) => (((acc[v.nodeName.toLowerCase() + "s"]) ? acc[v.nodeName.toLowerCase() + "s"].push(v) : acc[v.nodeName.toLowerCase() + "s"] = [v]), acc), {});
               for(let key in this.elements) {
                    if(this.elements[key].length === 1) this.elements[key] = this.elements[key][0];
               }
               this.webpage = this.head.parentElement;
               this.webpage.html = this.webpage.outerHTML;
               this.webpage.text = this.webpage.outerText;
               this.videos = (this.elements.videos) ? this.elements.videos : null;
               this.objectElements = (this.elements.objects) ? this.elements.objects : null;
               this.iframes = (this.elements.iframes) ? this.elements.iframes : null;
               this.videoExtentions = ["flv", "mp4", "m4v", "avi", "dvi", "wmv", "mp4", "mpg", "mpeg", "ogg", "m4v", "wmv", "mkv", "m3u8", "mp3"];
               this.mediaRex = new RegExp(`(?:"|')((?:https?:)\/\/(?:redirector.googlevideo.com\/videoplayback\?|[^"']+\\.(${this.videoExtentions.join("|")}))(?:[^"']+)?)(?:"|")`, "g");
               this.media = this.doc.documentElement.outerHTML.match(this.mediaRex);
               if(this.media) {
                    this.media = this.media.map(m => m.replace(/"/g, "").replace(/&amp;/g, "&"));
                    if(this.media.length === 1) this.media = this.media;
               }
               this.searchAllQuotedTxt = (item) => [...this.allQuotedTxt].reduce((acc, v) => (((new RegExp(item, "g").test(v)) ? acc.push(v) : null), acc), []);
          },
          CELS(items) {
               class CELS {
                    constructor(items) {
                         this.insertHTML.locations = {
                              beforebegin: "beforebegin",
                              afterbegin: "afterbegin",
                              beforeend: "beforeend",
                              afterend: "afterend"
                         }
                         var template = document.createElement('div');
                         if(typeof items === "object") {
                              template.innerHTML = items.html;
                              for(let key in items) {
                                   if((items.hasOwnProperty(key)) && (key !== "html")) {
                                        template.firstChild[key] = items[key];
                                   }
                              }
                         }
                         else {
                              template.innerHTML = items;
                         }
                         this[0] = template.firstChild;
                    }
                    item(i) {
                         return this[i];
                    }
                    insertHTML(where, html) {
                         this.item(0).insertAdjacentHTML(where, html);
                    }
                    appendElement(items) {
                         var template = new CELS(items);
                         this.item(0).appendChild(template[0]);
                    }
                    removeElement(i) {
                         this.item(0).childNodes[i].remove();
                    }
                    appendTo(element) {
                         element.appendChild(this.item(0));
                    }
               }
               return new CELS(items);
          },
          slang: {
               CapWords(words) {
                    return words.replace(/\w+/g, function (word) {
                         return word.charAt(0).toUpperCase() + word.slice(1).toLowerCase();
                    });
                    /*
                    return words.split(/\s/g).reduce((acc, w) => {
                         var isLastNonLetter = "", i = 0;
                         if(/\W$/.test(w)) {
                              isLastNonLetter = w.match(/\W$/)[0];
                         }
                         if(w.match(/(\w|\W\w)(\w+)/) && !/^(a|an|is|and|the|with|x+)$/.test(w.toLowerCase())) {
                              console.log(w.match(/^(\w|\W\w)(\w+)/)[1]);
                              acc.push(w.match(/^(\w|\W\w)(\w+)/)[1].toUpperCase() + w.match(/^(\w|\W\w)(\w+)/)[2].toLowerCase() + isLastNonLetter);
                         } else if(/^[x]+(\.|$)/.test(w.toLowerCase())) {
                              if(/\./.test(w)) {
                                   acc.push(w.split(/\./)[1].toUpperCase() + "." + w.split(/\./)[2].toLowerCase() + isLastNonLetter);
                              } else {
                                   acc.push(w.toUpperCase() + isLastNonLetter);
                              }
                         } else {
                              acc.push(w.toLowerCase() + isLastNonLetter);
                         }
                         i++;
                         return acc;
                    }, []).join(" ");
                    */
               },
               CapWordsInList(list) {
                    return list.split(/\n/g).reduce((acc, item) => {
                         acc += `${sDom.slang.CapWords(item.trim())}\n`;
                         return acc;
                    }, "");
               },
               PlusToUnderscore(pluses) {
                    return pluses.replace(/\+/g, "_");
               },
               caseover() {
                    if(!document.querySelector("#csscaseoverlay")) {
                         var caseoverlay = `#_caseul{list-style:none!important;padding:0!important;}`;
                         caseoverlay += `#_caseoverlay{position:fixed!important;display:none;width:100%!important;`;
                         caseoverlay += `top:0!important;left:0!important;right:0!important;bottom:0!important;`;
                         caseoverlay += `background-color:rgba(0,0,0,.5)!important;z-index:200000!important;}`;
                         caseoverlay += `#_casetext{top:50%!important;left:50%!important;height:80%!important;`;
                         caseoverlay += `width:80%!important;z-index:1!important;font-size:14px!important;`;
                         caseoverlay += `font-family:Arial!important;color:#000!important;background-color:#fff!important;`;
                         caseoverlay += `transform:translate(-50%,-50%)!important;-ms-transform:translate(-50%,-50%)!important;`;
                         caseoverlay += `text-shadow:none!important;}`;
                         caseoverlay += `#_caseexit,#_casetext,#_openLink{position:absolute!important;}`;
                         caseoverlay += `#_allcaseinput{position:relative;margin:0 auto;top:50%!important;line-height:24px!important;`;
                         caseoverlay += `z-index:20000000!important;font-size:15px!important;font-weight:300!important;`;
                         caseoverlay += `color:#000!important;cursor:pointer!important;text-align:center!important;}`;
                         caseoverlay += `#_caseinput{top:50%!important;width:60%!important;border:1px solid #000!important;`;
                         caseoverlay += `margin-right:5px!important;margin-left:5px!important;line-height:25px!important;`;
                         caseoverlay += `font-size:15px!important;font-weight:300!important;padding:5px!important;}`;
                         caseoverlay += `#_caseexit{top:5px!important;right:5px!important;z-index:20000000!important;`;
                         caseoverlay += `font-size:25px!important;font-weight:700!important;color:#fff!important;`;
                         caseoverlay += `cursor:pointer!important;}`;
                         caseoverlay += `#_casesubmit{line-height:25px!important;max-height:100%!important;padding:4px 15px!important;`;
                         caseoverlay += `border:3px double #fff!important;background-color:#000!important;color:#fff!important;}`;
                         caseoverlay += `#_casesubmit:hover{border:3px double #000!important;background-color:#fff!important;`;
                         caseoverlay += `color:#000!important;}`;
                         caseoverlay += `#_casesubmit:active{border:3px double #000!important;background-color:#000!important;color:#fff!important;}`;
                         caseoverlay = sDom.fixCssText(caseoverlay);
                         var csscaseText = document.createTextNode(caseoverlay);
                         var csscaseoverlay = document.createElement('style');
                         csscaseoverlay.appendChild(csscaseText);
                         csscaseoverlay.id = 'csscaseoverlay';
                         document.head.appendChild(csscaseoverlay);
                    }
                    if(!document.querySelector("#_caseoverlay")) {
                         var caseoverlayHTML = `<div id="_caseoverlay">
                              <div id="_casetext">
                                   <div id="_allcaseinput">
                                        <label for="_caseinput">Enter Text:</label>
                                        <input id="_caseinput" type="text" name="text">
                                        <input id="_casesubmit" type="submit" name="submit" text="Submit">
                                   </div>
                              </div>
                              <span id="_caseexit">&#x2717;</span>
                         </div>`;
                         var dcoh = sDom.CELS(caseoverlayHTML);
                         //var d = document.createElement("div");
                         //d.innherHTML = caseoverlayHTML;
                         //document.body.appendChild(d.firstChild);
                         dcoh.appendTo(document.body);
                         dcoh.item(0).style.display = "block";
                         document.querySelector("#_caseinput").value = document.title;
                         document.querySelector("#_casesubmit").onclick = function () {
                              var val = document.querySelector("#_caseinput").value;
                              val = sDom.slang.CapWords(val);
                              var ul = document.createElement("ul");
                              ul.id = "_caseul";
                              var li = document.createElement("li");
                              li.id = "_caseli";
                              li.textContent = val;
                              ul.appendChild(li);
                              document.querySelector("#_allcaseinput").appendChild(ul);
                         }
                         document.querySelector("#_caseoverlay").onclick = function (evt) {
                              //console.log(evt.target.id);
                              if(!/(_allcaseinput|_caseinput|_caseinput|_casesubmit|_casetext|_caseul|_caseli)/.test(evt.target.id)) {
                                   document.querySelector("#_caseoverlay").remove();
                              }
                         }
                    }
               }
          },
          titleCase() {
               return sDom.slang.CapWords(document.title);
          }
     }
     //sDom.RegexList.init();
     global.sDom = sDom;
     global.testLocation = sDom.testLocation;
     global.prompt = null;
})(window);