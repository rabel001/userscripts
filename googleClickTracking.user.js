// ==UserScript==
// @name            Google Click Tracking Removal
// @author          rabel001
// @version         3.9.20.2020
// @icon            http://google.com/favicon.ico
// @description     Remove onmousedown attribute of all links on google search
// @namespace       https://gitlab.com/rabel001/userscripts/blob/master/googleClickTracking.user.js
// @updateURL       https://glcdn.githack.com/rabel001/userscripts/raw/master/googleClickTracking.user.js
// @downloadURL     https://glcdn.githack.com/rabel001/userscripts/raw/master/googleClickTracking.user.js
// @include         *google*
// @exclude         *google*/maps*
// @noframes
// @grant           none
// ==/UserScript==

(() => {
     "use strict";

     //Set this to true for links to open in a new tab.
     var LINKS_TO_NEW_TAB = true;

     /*==============================*/
     /* ---Hexadecimal characters--- */
     /*==============================*/
     const icons = {
          'BallotBox': '&#x2611;',
          'Circle': '&#9899;',
          'ExitBox': '&#10062;',
          'FeatherArrow': '&#10163;',
          'Flag': '&#9873;',
          'HeavyAsterisk': '&#10033;',
          'HeavyCheckMark': '&#x2714;',
          'HeavyCheckBox': '&#9989;',
          'HeavyCrossMark': '&#10060;',
          'HeavyDashedArrow': '&#10144;',
          'HeavyGreekCross': '&#10010;',
          'HeavyHeart': '&#10084;',
          'HeavyX': '&#10006;',
          'LargeX': '&#10060;',
          'RightArrowHead': '&#10148;',
          'Shogi': '&#9751;',
          'Square': '&#x25A3;',
          'Star4': '&#10022;',
          'Star5': '&#9733;',
          'Star6': '&#10038;',
          'Star8': '&#10039;',
          'Star12': '&#10041;',
          'StarCircled': '&#10026;'
     };

     var inum = 0;

     function deleteData(a) {
          try {
               if(Object.entries(a.dataset).length > 0) {
                    Object.entries(a.dataset).forEach((set) => {
                         console.log("delet_data inum:", inum++);
                         console.log("set:", set);
                         console.log("get dataset:", a.getAttribute(`data-${set[0]}`));
                         a.removeAttribute(`data-${set[0]}`);
                         delete a.dataset[`${set[0]}`];
                         a.target = "_blank";
                         console.log("new a:", a);
                    });
               }
          } catch {}
     }

     function doDeleteJSaction() {
          document.querySelectorAll("[jsaction]").forEach((a) => {
               console.log("jsaction:", a.getAttribute("jsaction"));
               a.removeAttribute("jsaction");
               a.removeAttribute("jsdata");
               console.log("jsaction removed:", a);
          });
          console.log("data-ved:", document.querySelectorAll("[data-ved]"));
     }

     function doDataDelete() {
          var links = [...document.links].concat(document.querySelectorAll("a"));
          console.log("rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr", links.length);
          links.forEach((a) => {
               deleteData(a);
          });
          //doDeleteJSaction();
     }

     window.addEventListener('load', () => { setTimeout(doDataDelete, 500); });

     const isFound = (e) => !e.getAttribute('found');
     const flsIsFound = (e) => e.parentElement.tagName !== 'TD' && !e.getAttribute('found');
     const getHrefs = (e) => e.querySelector("a");


     var rl = document.querySelectorAll('a[onmousedown*="return rwt"]');
     for(var l = 0; l < rl.length; l++)
          rl[l].removeAttribute('onmousedown');
     const removeMouseDown = (e) => {
          e.removeAttribute('onmousedown');
          if(LINKS_TO_NEW_TAB) {
               e.setAttribute('target', '_blank');
          }
          return e;
     }
     const removePing = (e) => {
          e.querySelector("a").removeAttribute("ping");
          if(LINKS_TO_NEW_TAB) {
               e.querySelector("a").setAttribute('target', '_blank');
          }
          return e;
     }
     const addIcon = (e) => {
          var span = document.createElement('span');
          span.innerHTML = ` ${icons.BallotBox} `;
          span.id = "square";
          span.style.color = "red";
          e.insertAdjacentElement("afterbegin", span);
          e.setAttribute('found', true);
     }

     const observer = new MutationObserver((mutations) => {
          mutations.forEach((mutation) => {
               if(mutation.target.tagName === "A") {
                    deleteData(mutation.target);
               }
          });
          var tbm = Object.fromEntries(new URLSearchParams(location.search)).tbm;
          if(tbm && tbm === "nws") { //if(/(\?|&)tbm=nws&?/.test(location.search)) {
               [...document.querySelectorAll('.dbsr a')]
               .filter(isFound).map(removeMouseDown);
               [...document.querySelectorAll('.dbsr a div[style]')].filter((el) => { return el.textContent.length > 0 })
                    .filter(isFound).map(addIcon);
          } else {
               if(document.querySelectorAll(".rc .r")) {
                    [...document.querySelectorAll(".rc .r")]
                    .filter(isFound).filter(getHrefs).map(removePing).map(addIcon);
               }
               if(document.querySelectorAll("h3.r")) {
                    [...document.querySelectorAll("h3.r")]
                    .filter(isFound).filter(getHrefs).map(removeMouseDown).map(addIcon);
               }
               if(document.querySelectorAll('div.r')) {
                    [...document.querySelectorAll('div.r')]
                    .filter(isFound).filter(getHrefs).map(removeMouseDown).map(addIcon);
               }
               if(document.querySelectorAll('.fl')) {
                    [...document.querySelectorAll('.fl')]
                    .filter(flsIsFound).filter(removeMouseDown).map(addIcon);
               }
               if(document.querySelectorAll('a[onmousedown*="return rwt"]')) {
                    var rl = document.querySelectorAll('a[onmousedown*="return rwt"]');
                    [...rl].filter(isFound).map(removeMouseDown).map(addIcon);
                    //for (var l=0;l<rl.length;l++) {
                    //rl[l].removeAttribute('onmousedown');
                    //}
               }
               if(document.querySelector('a[ping]')) {
                    document.querySelectorAll('a[ping]').forEach(p => { p.removeAttribute("ping"); });
               }
          }
          /*
           mutations.forEach(function(mutation) {
                 //console.log(mutation.target);
              if (mutation.type == "attributes") {
                   console.log(mutation.attributeName);
                   if(mutation.target.tagName === "A" && mutation.attributeName === "onmousedown") {
                        setTimeout(() => {
                             mutation.target.onmousedown = null;
                             mutation.target.setAttribute("onmousedown", null);
                             console.log(mutation);

                           //mutation.target.setAttribute("onmousedown", "return void 0;")
                        }, 1000);

                        console.log("HEEEEEEEEEEEEEEEEEEEEEEEERRRRRRRRRRREEEEEEEEEEEEEEEEE!!!!!!!!!!!!");
                   }
              }
            });
          */
     });

     var targetNode = document.body;
     var observerConfig = {
          childList: true,
          subtree: true,
          attributes: true
     };
     observer.observe(targetNode, observerConfig);

     /*
      * 
      * From https://gitlab.com/KevinRoebert/ClearUrls/-/blob/master/core_js/google_link_fix.js
      *
      */

     function injectFunction() {
          let ele = document.createElement('script');
          let s = document.getElementsByTagName('script')[0];
          console.log("s:", s);
          ele.type = 'text/javascript';
          ele.textContent = "Object.defineProperty(window, 'rwt', {" +
               "    value: function() { return true; }," +
               "    writable: false," +
               "    configurable: false" +
               "});";

          s.parentNode.insertBefore(ele, s);
     }

     /*
      * The main entry
      */
     function main() {
          injectFunction();

          document.addEventListener('mouseover', function(event) {
               let a = event.target,
                    depth = 1;

               while(a && a.tagName !== 'A' && depth-- > 0) {
                    a = a.parentNode;
               }

               if(a && a.tagName === 'A') {
                    try {
                         //console.log("a.dataset entries length:", Object.entries(a.dataset));
                         if(Object.entries(a.dataset).length > 0) {
                              Object.entries(a.dataset).forEach((set) => {
                                   console.log("set:", set);
                                   console.log("get dataset:", a.getAttribute(`data-${set[0]}`));
                                   a.removeAttribute(`data-${set[0]}`);
                                   delete a.dataset[`${set[0]}`];
                                   console.log("new a:", a);
                              });
                              //console.log("a.dataset:", a.dataset);
                              //console.log("a.dataset length:", a.dataset);
                         }
                         //console.log("data-ved:", a.getAttribute("data-ved"));
                         //a.removeAttribute('data-ved');
                         //delete a.dataset.ved;
                         //console.log("data-cthref:", a.getAttribute("data-cthref"));
                         a.removeAttribute('data-cthref');
                         delete a.dataset.cthref;
                    } catch (e) {
                         console.log(e);
                    }
               }
          }, true);
     }

     main();

})();