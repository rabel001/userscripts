// ==UserScript==
// @name            Facebook Videos Page Disabler
// @author          rabel001
// @version         1.00
// @description     Sets videos to paused and turns off autoplay
// @include         *facebook.com/watch/*
// @grant           unsafeWindow
// ==/UserScript==

(() => {
	'use strict';

    var LOGIT = false;
	var observer = new MutationObserver(function(mutation_records) {
		var usw = (!!unsafeWindow) ? unsafeWindow : window;
		for(var mutation of mutation_records) {
			if(mutation.target instanceof HTMLMediaElement) {
				if(mutation.target.tagName === "VIDEO") {
					if(LOGIT) usw.console.log({'Mutation': mutation.target});
					mutation.target.autoplay = false;
					mutation.target.pause();
				}
			}
			for(let inner_mutation of Array.from(mutation.addedNodes)) {
				if(inner_mutation instanceof HTMLMediaElement) {
					if(inner_mutation.tagName === "VIDEO") {
						if(LOGIT) usw.console.log({'Inner mutation': inner_mutation});
						inner_mutation.autoplay = false;
						inner_mutation.pause();
					}
				}
			}
		}
	});
	observer.observe(document, {
		childList: true,
		attributes: true,
		characterData: true,
		subtree: true
	});
})();