// ==UserScript==
// @name              Dallas News
// @author            rabel001
// @version           1.1.23.19
// @include           *dallasnews.com*
// @namespace         DallasNews
// @description       Delete overlay and add overflow scroll for assinign incognito blocking
// @icon              https://www.dallasnews.com/resources/motif/images/favicon-16x16.png
// @noframes
// @grant             none
// ==/UserScript==

(() => {
	"use strict";

	if(!!document.querySelector('#courier-iframe')) {
		document.querySelector('#courier-iframe').remove();
		document.querySelector('#top').style.overFlow = 'auto';
	}
})();