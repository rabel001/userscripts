// ==UserScript==
// @name            Password Generator
// @author          rabel001
// @homepage        https://rabel001.gitlab.io
// @version         1.1.13.20
// @description     Opens webbased password generator in a new window
// @namespace       https://gitlab.com/rabel001/userscripts/blob/master/Password.Generator.user.js
// @updateURL       https://gl.git.rest/rabel001/userscripts/raw/master/Password.Generator.user.js
// @updateURL       https://gl.git.rest/rabel001/userscripts/raw/master/Password.Generator.user.js
// @resource        icon //iconshow.me/media/images/System/plex-icons/png/Utilities/512/password-manager.png
// @include         *
// @noframes        
// @grant           GM_openInTab
// @grant           GM_registerMenuCommand
// @grant           GM_getResourceURL
// ==/UserScript==

/*
 *
 * https://githack.com is blocked at businesses.
 * Using https://raw.git.rest instead.
 * https://gl.githack.com/rabel001/userscripts/raw/master/Password.Generator.user.js
 *
 */

(() => {
     "use strict";

     GM_registerMenuCommand("Password Generator", () => {
          GM_openInTab("http://blank.org/#password-generator");
     });

     if(location.href !== "http://blank.org/#password-generator") {
          return;
     }

     var _bstrp = document.createElement("link");
     _bstrp.href = "https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css";
     _bstrp.rel = "stylesheet";
     document.head.appendChild(_bstrp);

     const cssText = `
     <style id="passgen-css">
          html,
          body {
               width: 100%;
               height: 80%;
               margin: 0;
               padding: 0;
          }
          
          .passgen-container {
               margin: auto;
               margin-top: 30px;
               max-width: 600px;
               background-color: white;
               padding: 15px;
               border-radius: .25rem;
               border: 1px solid #ced4da;
          }
          
          .form-container {
               background-color: #F8F8F8;
               border: 1px #F0F0F0 solid;
               padding: 10px;
               margin-bottom: 20px;
          }
          
          .unsupported-browser-alert {
               display: none;
          }
          
          details {
               margin-left: 20px;
          }
          
          .input-group-button {
               display: table-cell;
          }
          
          .clippy {
               border: 0;
          }
          
          #password-input {
               border-top-left-radius: 0 !important;
               border-bottom-left-radius: 0 !important;
          }
          
          #copy-text {
               width: 35px;
               height: 100%;
               display: inline-block;
               padding: 6px 12px;
               font-size: 13px;
               font-weight: 700;
               line-height: 20px;
               color: #333;
               white-space: nowrap;
               vertical-align: middle;
               cursor: pointer;
               border: 1px solid #d5d5d5;
               border-top-left-radius: 3px;
               border-bottom-left-radius: 3px;
               -webkit-user-select: none;
               -moz-user-select: none;
               -ms-user-select: none;
               user-select: none;
               -webkit-appearance: none;
               text-transform: none;
               background: url(https://upload.wikimedia.org/wikipedia/commons/thumb/9/91/Octicons-clippy.svg/525px-Octicons-clippy.svg.png) no-repeat center top;
               background-size: 13px;
               background-position: center;
               background-color: #d5d5d5;
          }
          
          #copy-text:hover {
               background: url(https://upload.wikimedia.org/wikipedia/commons/thumb/9/91/Octicons-clippy.svg/525px-Octicons-clippy.svg.png) no-repeat center top;
               background-size: 13px;
               background-position: center;
               background-color: lightslategray;
               cursor: pointer;
          }
          
          #generate-password-button {
               border: 1px solid #ced4da !important;
          }
          
          #generate-password-button:hover {
               cursor: pointer;
               background: lightslategray;
               color: white;
          }
          
          .copied::after {
               position: absolute;
               top: 0;
               left: 38px;
               z-index: 10000000000;
               display: block;
               content: "Copied";
               padding: 9px;
               color: #fff;
               background-color: black;
               border-radius: 3px;
               opacity: 0;
               will-change: opacity, transform;
               animation: showcopied 1.5s ease;
          }
          
          /* Safari 4.0 - 8.0 */
          @-webkit-keyframes showcopied {
               100% {
                    opacity: 0;
                    transform: translateX(500%);
          
               }
          
               0% {
                    opacity: 1;
                    transform: translateX(0);
               }
          }
          
          @keyframes showcopied {
               100% {
                    opacity: 0;
                    transform: translateX(500%);
          
               }
          
               0% {
                    opacity: 1;
                    transform: translateX(0);
               }
          }
          
          h1 {
               text-align: center;
          }
          
          hidden {
               display: none;
          }
          show {
               display: block;
          }
     </style>`;

     if(!document.getElementById("passgen-css")) {
          var _style = document.createElement("style");
          _style.id = "passgen-css";
          _style.textContent = cssText;
          document.head.appendChild(_style);
     }

     const htmlText = `
     <div class="passgen-container" style="display: none;">
          <h1>Secure Password Generator</h1>
          <noscript>
               <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> <strong>Hold
                         on.</strong> It looks like you have disabled JavaScript. Unfortunately, this tool will not work
                    until you enable it.</div>
          </noscript>
          <div class="alert alert-danger unsupported-browser-alert" id="unsupported-browser-alert">
               <p>
                    <span class="glyphicon glyphicon-warning-sign"></span> <strong>Hold on.</strong> It looks like you are
                    using an <em>unsupported browser</em> (probably Internet Explorer...) Unfortunately, this tool will not
                    work until you upgrade your browser. Sorry for any inconvenience caused.
               </p>
          </div>
          <div class="alert alert-info" role="alert">
               <p>Use this online tool to generate a strong and random password. The available characters in each set are
                    user friendly - there are no ambiguous characters such as i, l, 1, o, 0, etc. Password generation is done
                    on the client-side meaning no one has access to the passwords you generate here.</p>
          </div>
          <div class="form-container clearfix">
               <div class="form-group">
                    <label for="input-group">Secure Password:</label>
                    <div class="input-group" id="input-group">
                         <span class="input-group-button">
                              <button class="btn2" id="copy-text" title="Copy to clipboard"
                                   data-copytarget="#password-input"></button>
                         </span>
                         <input type="text" class="form-control" id="password-input" onclick="this.focus();this.select()"
                              readonly="">
                         <span class="input-group-btn">
                              <button class="btn btn-default" id="generate-password-button" type="button">
                                   <span class="glyphicon glyphicon-refresh"></span> Generate Another
                              </button>
                         </span>
                    </div>
               </div>
               <div class="form-group">
                    <label>Password Length:</label>
                    <select id="password-length" class="form-control"></select>
               </div>
               <details>
                    <summary>Advanced Options</summary>
                    <div id="advancedOptions">
                         <br>
                         <label>
                              <input type="checkbox" id="include-uppercase-chars-checkbox" checked=""> Upper-case
                         </label>
                         <label>
                              <input type="checkbox" id="include-lowercase-chars-checkbox" checked=""> Lower-case
                         </label>
                         <label>
                              <input type="checkbox" id="include-numbers-checkbox" checked=""> Numbers
                         </label>
                         <label>
                              <input type="checkbox" id="include-special-chars-checkbox" checked=""> Special
                         </label>
                    </div>
                    <span class="glyphicon glyphicon-menu-hamburger"></span>
               </details>
          </div>
     </div>`;

     if(!document.getElementById("passgen-container")) {
          document.body.innerHTML = htmlText;
          const operations = {
               createSelect() {
                    //Other ways to create an array of 101 chars and iterate each
                    //".".repeat(101).split("").forEach((v, i) => { //do some work with i as integer });
                    //Array(101).fill(1).forEach((v, i) => { //do some work with i as integer });
                    for(let i = 5; i < 101; i++) {
                         if(i > 4) {
                              var newoption = document.createElement('option');
                              newoption.value = i;
                              newoption.text = i;
                              if(i === 32) {
                                   newoption.selected = true;
                              }
                              document.querySelector("select").appendChild(newoption);
                         }
                    }
               },
               passwordGenerator(options) {
                    const uppercase = "ABCDEFGHJKMNPQRSTUVWXYZ";
                    const lowercase = "abcdefghjkmnpqrstuvwxyz";
                    const numbers = "1234567890";
                    const special = "!\#$%&()*+,-./:;<=>?@[\]^_\`{|}~";
                    //var special = "!@#$%&*?";
                    let candidates = '';
                    if(options.includeUppercaseChars) {
                         candidates += uppercase;
                    }
                    if(options.includeLowercaseChars) {
                         candidates += lowercase;
                    }
                    if(options.includeNumbers) {
                         candidates += numbers;
                    }
                    if(options.includeSpecialChars) {
                         candidates += special;
                    }
                    var password = "";
                    for(let x = 0; x < options.passwordLength; x++) {
                         let i = Math.floor(Math.random() * candidates.length);
                         password += candidates.charAt(i);
                    }
                    return password;
               },
               getOptions() {
                    return {
                         passwordLength: document.querySelector("#password-length").value,
                         includeUppercaseChars: document.querySelector("#include-uppercase-chars-checkbox").checked,
                         includeLowercaseChars: document.querySelector("#include-lowercase-chars-checkbox").checked,
                         includeNumbers: document.querySelector("#include-numbers-checkbox").checked,
                         includeSpecialChars: document.querySelector("#include-special-chars-checkbox").checked,
                    };
               },
               outputGeneratedPassword() {
                    var password;
                    try {
                         password = operations.passwordGenerator(operations.getOptions());
                    } catch (ex) {
                         //document.querySelector("#unsupported-browser-alert").style.display = 'block';
                         //return;
                    }
                    if(password === "") {
                         alert("Whoops. You unselected all the options. I don't know what characters you want. Click on the button entitled \"Advanced Options\" and enable some options then try again. Nice one");
                         return;
                    }
                    document.querySelector("#password-input").value = password;
                    document.querySelector("#password-input").blur();
               },
               copyToClipboard(e) {
                    var t = e.target,
                         c = t.dataset.copytarget,
                         inp = (c) ? document.querySelector(c) : null;

                    if(inp && inp.select) {
                         inp.select();
                         try {
                              // copy text
                              document.execCommand('copy');
                              inp.blur();

                              // copied animation
                              t.classList.add('copied');

                              setTimeout(function() {
                                   t.classList.remove('copied');
                              }, 1500);
                         } catch (ex) {
                              alert('Please press Ctrl+C to copy.');
                         }
                    }
               }
          };
          operations.createSelect();
          operations.outputGeneratedPassword();
          document.querySelector("#generate-password-button").addEventListener('click', operations.outputGeneratedPassword);
          document.querySelector('#copy-text').addEventListener('click', operations.copyToClipboard);
          document.querySelector(".passgen-container").style.display = "block";
          document.title = "Password Generator";
          var _icon = document.createElement("link");
          _icon.rel = "shortcut icon";
          _icon.href = GM_getResourceURL("icon");
          document.head.appendChild(_icon);
     }
})();