// ==UserScript==
// @name              Virustotal URL Scan
// @author            rabel001
// @homepage          https://rabel001.gitlab.io
// @version           1.1.14.19
// @icon              https://www.virustotal.com/favicon.ico
// @description       Scan URLs using the Virustotal API
// @namespace         https://gitlab.com/rabel001/userscripts/blob/master/VirustotalUrlScan.user.js
// @updateURL         https://glcdn.githack.com/rabel001/userscripts/raw/master/VirustotalUrlScan.user.js
// @downloadURL       https://glcdn.githack.com/rabel001/userscripts/raw/master/VirustotalUrlScan.user.js
// @include           *
// @noframes
// @grant             GM_xmlhttpRequest
// @grant             unsafeWindow
// @run-at            document-start
// ==/UserScript==

;((global, CreateHTMLElement) => {
     global.vtContainer = () => {
          if(!document.querySelector("#vt-overlay")) {
               global.virustotal = {
                    apikey: "26c2e6f73ca56321b60df2f02b92bec014196d1b91ad8345db3db82a0c1630bc",
                    scanUrl: "https://www.virustotal.com/vtapi/v2/url/scan",
                    reportUrl: "https://www.virustotal.com/vtapi/v2/url/report",
                    get(url, data) {
                         return new Promise((resolve) => {
                              var arr = [];
                              for(var e in data) {
                                   if(data.hasOwnProperty(e)) {
                                        arr.push(e + '=' + encodeURIComponent(data[e]));
                                   }
                              }
                              data = arr.join("&");
                              GM_xmlhttpRequest({
                                   method: data ? "POST" : "GET",
                                   url: url,
                                   data: data,
                                   headers: {
                                        "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
                                   },
                                   onload: (response) => {
                                        //if(response.readyState === 4 && response.status === 200) {
                                        resolve(response.responseText);
                                        //}
                                   },
                                   onerror: (ex) => {
                                        resolve(ex);
                                   }
                              });
                         });
                    },
                    sleep(time) {
                         return new Promise((resolve) => setTimeout(resolve, time));
                    }
               };
               if(!document.querySelector("#vt-overlay-css")) {
                    var cssText = `#vt-overlay{height: 100% !important;}`;
                    cssText += `#vt-exit,#vt-inner-container{position: absolute !important;}`;
                    cssText += `#vt-overlay {position: fixed !important;display: none;width: 100% !important;top: 0 !important;left: 0 !important;right: 0 !important;bottom: 0 !important;background-color: rgba(0, 0, 0, .5) !important;z-index: 200000 !important;}`;
                    cssText += `#vt-inner-container {top: 50% !important;left: 50% !important;height: 90% !important;width: 80% !important;z-index: 1 !important;font-size: 14px !important;font-family: Arial !important;color: #000 !important;background-color: #fff !important;transform: translate(-50%, -50%) !important;-ms-transform: translate(-50%, -50%) !important;}`;
                    cssText += `#vt-exit {top: 5px !important;right: 5px !important;z-index: 20000000 !important;font-size: 25px !important;font-weight: 700 !important;color: #fff !important;cursor: pointer !important;}`;
                    cssText += `#vt-text-container {padding-top: 20px !important;width: 80% !important;height: 90% !important;margin: auto !important;}`;
                    cssText += `#vt-text-container h3 {border-bottom: 1px solid black !important; width: 100% !important; margin-bottom: 5px !important; font: 400 1.17em Arial !important;color: black !important;}`
                    cssText += `#vt-textbox {width: 100% !important;display: grid !important;margin-bottom: 5px !important;border: 1px solid black !important;height: 19px !important;padding: 0px !important;font: 400 13.3333px Arial !important;}`;
                    cssText += `#vt-submit-btn {float: right;margin-bottom: 25px !important;}`;
                    cssText += `#vt-textbox-label {margin-bottom: 5px !important;}`;
                    cssText += `#vt-output {width: 100% !important; height: calc(95% - 140px) !important; overflow-x: hidden !important;}`;
                    cssText += `#vt-output-table {width:100% !important;border-collapse: collapse !important;color: black;font-size: 14px !important;font-family: Arial !important;}`;
                    cssText += `#vt-output-table td:nth-child(2),#vt-output-table td:nth-child(3) {text-align: center !important;}`;
                    cssText += `#vt-output-table td:nth-child(1),#vt-output-table td:nth-child(2){width: 33% !important;}`;
                    cssText += `#vt-output-table thead {background-color: lightsteelblue !important;}`;
                    cssText += `#vt-output-table th,#vt-output-table td {border: 1px solid black !important;}`;
                    cssText += `#vt-output-table th {text-align: center !important; font-weight: bold !important;}`;
                    cssText += `#vt-output-table tbody tr:nth-child(odd) {background-color: #f1f0f0 !important;}`;
                    cssText += `#vt-output-table tbody tr:hover {background-color: #d7d7ff !important;}`;
                    cssText += `#vt-output-table caption {border-top: 1px solid black !important;border-left:1px solid black !important;border-right:1px solid black !important;background-color: black !important; color: white !important;height: 20px !important;padding: 0px !important;line-height: 20px !important;text-align: center !important;}`;
                    document.head.appendChild(CreateHTMLElement(`<style id="vt-overlay-css" type="text/css">${cssText}</style>`));
               }

               var vtInnerContainer = CreateHTMLElement(`<div id="vt-inner-container" name="text"></div>`);
               var vtTextContainer = CreateHTMLElement(`<div id="vt-text-container"><h3>Virustotal URL Scan</h3></div>`);
               vtTextContainer.innerHTML += `<div id="vt-textbox-label">Enter url:</div><input id="vt-textbox" type="text" name="vt-textbox" value="${location.href}">`;
               var vtSubmitBtn = CreateHTMLElement({
                    html: `<input id="vt-submit-btn" name="vt-submit-btn" type="submit" value="Submit">`,
                    fn: () => {
                         var resource = document.querySelector("#vt-textbox").value;
                         console.log(resource);
                         virustotal.get(virustotal.scanUrl, {
                              url: resource,
                              apikey: virustotal.apikey
                         }).then((report) => {
                              function getReport() {
                                   virustotal.get(virustotal.reportUrl, {
                                        resource: resource,
                                        apikey: virustotal.apikey
                                   }).then((response) => {
                                        console.log(response);
                                        var GOOD_RESPONSE = false;
                                        try {
                                             JSON.parse(response).response_code;
                                             GOOD_RESPONSE = true;
                                        } catch (ex) {
                                             virustotal.sleep(3000).then(() => getReport());
                                             return;
                                        }
                                        if(GOOD_RESPONSE) {
                                             if(JSON.parse(response).response_code === 0) {
                                                  virustotal.sleep(3000).then(() => getReport());
                                             } else {
                                                  response = JSON.parse(response);
                                                  var table = CreateHTMLElement(`<table id="vt-output-table"><caption>${document.querySelector("#vt-textbox").value}</caption><thead><tr><th>Site</th><th>Detected</th><th>Result</th></tr></thead></table>`);
                                                  var tbody = CreateHTMLElement('<tbody></tbody>');
                                                  var scans = response.scans;
                                                  for(let scan in scans) {
                                                       if(scans.hasOwnProperty(scan)) {
                                                            var rcolor = "";
                                                            if(scans[scan].result.toUpperCase() === "CLEAN SITE") {
                                                                 rcolor = "green";
                                                            } else if(scans[scan].result.toUpperCase() === "UNRATED SITE") {
                                                                 rcolor = "orange";
                                                            } else {
                                                                 rcolor = "red";
                                                            }
                                                            var dcolor = (scans[scan].detected) ? "red" : "black";
                                                            var tr = CreateHTMLElement(`<tr><td>${scan}</td><td style="color:${dcolor}">${scans[scan].detected.toString().toUpperCase()}</td><td style="color:${rcolor};">${scans[scan].result.toUpperCase()}</td></tr>`);
                                                            tbody.appendChild(tr);
                                                       }
                                                  }
                                                  table.appendChild(tbody);
                                                  var vtOutput = document.querySelector("#vt-output");
                                                  if(document.querySelector("#vt-output-table")) {
                                                       document.querySelector("#vt-output-table").replaceWith(table);
                                                  } else {
                                                       vtOutput.appendChild(table);
                                                  }
                                                  vtOutput.style.overflowY = "scroll";
                                             }
                                        }
                                   });
                              }
                              getReport();
                         });
                    }
               });
               vtTextContainer.appendChild(vtSubmitBtn);
               var vtOutput = CreateHTMLElement(`<div id="vt-output"></div>`);
               vtTextContainer.appendChild(vtOutput);
               vtInnerContainer.appendChild(vtTextContainer);
               var vtExit = CreateHTMLElement({
                    html: `<span id="vt-exit">&#x2717;</span>`,
                    fn: () => {
                         document.getElementById("vt-overlay").remove();
                    }
               });
               var vtOverlay = CreateHTMLElement({
                    html: `<div id="vt-overlay" style="display: block;"></div>`,
                    fn: (evt) => {
                         if(evt.target.id === "vt-overlay") {
                              //if(evt.srcElement.tagName === 'DIV') {
                              document.getElementById("vt-exit").click();
                         }
                    }
               });
               vtOverlay.appendChild(vtInnerContainer);
               vtOverlay.appendChild(vtExit);
               document.body.appendChild(vtOverlay);
          }
     }
     document.onmouseup = function() {
          if(global.getSelection().anchorNode.nodeName === "#text" && global.getSelection().anchorNode.parentElement.tagName === "A") {
               var href = global.getSelection().anchorNode.parentElement.cloneNode();
               global.postMessage(href.href, location.origin);
          }
     };
})(
     (typeof unsafeWindow !== "undefined") ? unsafeWindow : window,
     function(items) {
          var html = (typeof(items) === "string") ? items : items.html;
          var fn = (typeof(items) === "object") ? items.fn : null;
          var template = document.createElement("template");
          template.innerHTML = html;
          template.content.firstChild.onclick = fn;
          return template.content.firstChild;
     }
);