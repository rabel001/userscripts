// ==UserScript==
// @name            TopRight Menu
// @author          rabel001
// @homepage        https://rabel001.gitlab.io
// @version         3.1.14.19
// @description     Menu with Password Generate Button and Bookmarklet encryption
// @namespace       https://gitlab.com/rabel001/userscripts/blob/master/TopRight-Menu.3.1.14.19.user.js
// @updateURL       https://gl.git.rest/rabel001/userscripts/raw/master/TopRight-Menu.3.1.14.19.user.js
// @downloadURL     https://gl.git.rest/rabel001/userscripts/raw/master/TopRight-Menu.3.1.14.19.user.js
// @include         *
// @noframes
// @grant           GM_registerMenuCommand
// @grant           unsafeWindow
// ==/UserScript==

/*
 *
 * https://githack.com is blocked at businesses.
 * Using https://raw.git.rest instead.
 * https://gl.githack.com/rabel001/userscripts/raw/master/TopRight-Menu.3.1.14.19.user.js
 *
 */

;((global) => {
     "use strict";

     var _cssStr = `#_details,#_summary {cursor: pointer !important;}`;
     _cssStr += `#_details,._href {font-family: arial, sans-serif !important;font-size: 14px !important;}`;
     _cssStr += `#_bmoverlay,#_details,._href {top: 0 !important;color: #fff !important;}`;
     _cssStr += `._bttn:hover,._href {text-decoration: none !important;}`;
     _cssStr += `#_details {position: fixed !important;right: 0 !important;z-index: 2147483646 !important;background: #000 !important;opacity: .5 !important;line-height: 18px !important;text-align: left !important;padding: 4px !important;border: 1px solid #ccc !important;}`;
     _cssStr += `#_details>div,#_summary {text-align: center !important;}`;
     _cssStr += `#_summary {background: #dbdbdb !important;color: #000 !important;padding: .2em .5em !important;}`;
     _cssStr += `#_opencontainer {width: 100% !important;}`;
     _cssStr += `._span {display: block !important;width: 90% !important;margin: 5px auto 0 !important;}`;
     _cssStr += `#_bmoverlay {position: absolute !important;left: 0 !important;z-index: 1000000 !important;width: 100% !important;padding-top: 20px !important;background-color: #000 !important;font-family: Arial !important;font-size: 16px !important;}`;
     _cssStr += `._buttons,._input,._txtarea {display: block !important;width: 90% !important;}`;
     _cssStr += `._txtarea {color: #000 !important;background-color: #fff !important;line-height: 24px !important;font: 14px Arial !important;padding-left: 5px !important;margin: 0 auto 10px !important;}`;
     _cssStr += `._bttn,._bttn:hover {color: #fff !important;border: 0 !important;}`;
     _cssStr += `._bttn {margin-right: 5px !important;font-weight: 700 !important;background-color: #444 !important;outline: 0 !important;padding: 10px 35px 9px !important;margin-bottom: 0 !important;margin-top: 0 !important;margin-left: 0 !important;letter-spacing: 0 !important;font-size: 14px !important;}`;
     _cssStr += `._bttn:hover {background-color: #999 !important;box-shadow: none !important;-webkit-box-shadow: none !important;}`;
     _cssStr += `._input,.new-hr {background-color: #fff !important;}`;
     _cssStr += `#_cpylbl {vertical-align: bottom !important;font-weight: inherit !important;}`;
     _cssStr += `._input {color: #000 !important;line-height: 24px !important;height: 24px !important;font: 14px Arial !important;padding-left: 5px !important;margin: 0 auto !important;}`;
     _cssStr += `._buttons {margin: auto !important;}`;
     _cssStr += `.new-hr {border: 0 !important;margin: 2px !important;height: 1px !important;}`;
     var _css = document.createElement('style');
     _css.type = 'text/css';
     var _cssTextNode = document.createTextNode(_cssStr);
     _css.appendChild(_cssTextNode);
     _css.id = "_css";
     document.head.appendChild(_css);
     var _details = document.createElement('details');
     _details.id = "_details";
     var _summary = document.createElement('summary');
     _summary.id = '_summary';
     _summary.innerText = 'Menu';
     _details.appendChild(_summary);
     /**************************/
     /***       Popup        ***/
     /**************************/
     var _popup = document.createElement('div');
     _popup.innerText = "Popup";
     _details.appendChild(_popup);
     _popup.addEventListener('click', () => {
          //debugger;
          var ff = !global.chrome;
          if(ff) {
               var flash = document.querySelectorAll("[type='application/x-shockwave-flash']");
               for(let i = 0, len = flash.length; i < len; i++) {
                    flash[i].setAttribute("wmode", "opaque");
                    flash[i].style.display = "none";
               }
               setTimeout(() => {
                    for(let i = 0, len = flash.length; i < len; i++) {
                         flash[i].style.display = "";
                    }
               }, 100 * flash.length);
          }
          var ntf = document.createElement("div");
          ntf.id = "topntf";
          var pw = document.documentElement.scrollWidth || document.body.scrollWidth;
          var ph = document.documentElement.scrollHeight || document.body.scrollHeight;
          var ptop = document.documentElement.scrollTop || document.body.scrollTop;
          var ntfStyle = "position: absolute;";
          ntfStyle += "top: 0px;";
          ntfStyle += "left: 0px;";
          ntfStyle += "z-index: 2147483647;";
          ntfStyle += "background-color: #000;";
          ntfStyle += "opacity: 0.8;";
          ntfStyle += "font-size: 50pt;";
          ntfStyle += "line-height: 50pt;";
          ntfStyle += "text-align: center;";
          ntfStyle += "color:#fff;";
          ntfStyle += "width: " + pw + "px;";
          ntfStyle += "height: " + ph + "px;";
          ntfStyle += "padding-top: " + ptop + "px;";
          ntf.setAttribute("style", ntfStyle);
          ntf.innerHTML = "please click the element to pop out";
          document.body.appendChild(ntf);
          document.addEventListener('mousedown', (e) => {
               if(document.getElementById("topntf")) {
                    document.getElementById("topntf").remove();
                    var target = document.elementFromPoint(e.clientX, e.clientY);
                    global.console.log({
                         "popup target": target
                    });
                    var ch = global.getComputedStyle(target, null).getPropertyValue("height");
                    var cw = global.getComputedStyle(target, null).getPropertyValue("width");
                    var ptarget = target;
                    var player = ptarget.cloneNode(true);
                    var cst = "width:100%  !important;";
                    cst += "height:100%  !important;";
                    cst += "max-width:100%  !important;";
                    cst += "margin:0px  !important;";
                    cst += "padding:0px  !important;";
                    cst += "top:0px  !important;";
                    cst += "left:0px  !important;";
                    cst += "z-index: 2147483646  !important;";
                    cst += "overflow:hidden  !important;";
                    cst += "position:fixed  !important;";
                    cst += "display: block  !important;";
                    cst += "visibility: visible  !important;";
                    player.setAttribute("style", cst);
                    while(global.getComputedStyle(target.parentNode, null).getPropertyValue("height") == ch && global.getComputedStyle(target.parentNode, null).getPropertyValue("width") == cw) {
                         ptarget = target.parentNode.cloneNode(true);
                         ptarget.replaceChild(player.cloneNode(true), ptarget.childNodes[Array.prototype.indexOf.call(target.parentNode.childNodes, target)]);
                         ptarget.setAttribute("style", cst);
                         player = ptarget;
                         target = target.parentNode;
                    }
                    var wurl = global.location.href;
                    if(ff) {
                         wurl = "about:blank";
                    }
                    var w = global.open(wurl, "_blank", "width=520,height=325,left=" + (screen.availWidth - 525) + ",top=" + (screen.availHeight - 385));
                    if(!w) {
                         global.alert("Please set to allow pop-up windows ");
                    } else {
                         var id = global.setInterval(() => {
                              global.windocbody = w.document.body;
                              if(w.document.body.childElementCount > 0 || wurl == "about:blank") {
                                   global.clearInterval(id);
                                   w.document.documentElement.innerHTML = " ";
                                   w.document.documentElement.replaceChild(document.head.cloneNode(true), w.document.head);
                                   w.document.body.setAttribute("style", cst);
                                   w.document.body.appendChild(player);
                                   var rt = document.createElement('script');
                                   var _popupStyle = "position: fixed;";
                                   _popupStyle += "top: 5px;";
                                   _popupStyle += "right: 10px;";
                                   _popupStyle += "z-index: 2147483647;";
                                   _popupStyle += "background: #000;";
                                   _popupStyle += "opacity: 0.3;";
                                   _popupStyle += "color:#fff;";
                                   _popupStyle += "font-size: 8pt;";
                                   _popupStyle += "cursor: pointer;";
                                   _popupStyle += "text-align: center;";
                                   _popupStyle += "color:#fff;";
                                   _popupStyle += "padding: 4px;";
                                   rt.type = "text/javascript";
                                   rt.innerHTML = `var ck=document.createElement("div");`;
                                   rt.innerHTML += `_popup.setAttribute("style", "${_popupStyle}");`;
                                   rt.innerHTML += `_popup.innerHTML="return";`;
                                   rt.innerHTML += `_popup.onclick= () -> {`;
                                   rt.innerHTML += `  window.open("${global.location.href}");`;
                                   rt.innerHTML += `  window.close();`
                                   rt.innerHTML += `}`;
                                   rt.innerHTML += `document.body.appendChild(ck);`;
                                   w.document.body.appendChild(rt);
                              }
                         }, 100);
                    }
               }
          }, false);
     }, false);
     /**********************/
     /*** Isolate Player ***/
     /**********************/
     var _isolatePlayer = document.createElement('div');
     _isolatePlayer.innerText = "Isolate Player";
     _details.appendChild(_isolatePlayer);
     _isolatePlayer.addEventListener('click', () => {
          var isolate = (which) => {
               return new Promise(resolve => {
                    var isoleme = document.querySelector("video") || document.querySelector('embed[type^="video/"]') || document.querySelector('object[data*=swf]');
                    switch (which) {
                         case 1:
                              isoleme = isoleme.parentElement;
                              break;
                         case 2:
                              isoleme.parentElement.classList.remove('vviidd');
                              isoleme = isoleme.parentElement.parentElement;
                              //Array.from(isoleme.children).forEach(e => { e.style.height = '100%'; });
                              break;
                         case 3:
                              isoleme.parentElement.parentElement.classList.remove('vviidd');
                              isoleme = isoleme.parentElement.parentElement.parentElement;
                              //Array.from(isoleme.children).forEach(e => { e.style.height = '100%'; });
                              break;
                         case 4:
                              isoleme.parentElement.parentElement.parentElement.classList.remove('vviidd');
                              isoleme = isoleme.parentElement.parentElement.parentElement.parentElement;
                              //Array.from(isoleme.children).forEach(e => { e.style.height = '100%'; });
                              break;
                    }
                    isoleme.classList.add('vviidd');
                    let isoCSSText = `.vviidd{position:fixed !important;left:0 !important;z-index:10000000 !important;padding-top:0 !important;width:calc(100% - 0px) !important;height:calc(100% - 0px) !important}`;
                    var isoStyle = document.createElement('style');
                    isoStyle.type = 'text/css';
                    var isoCSSTextNode = document.createTextNode(isoCSSText);
                    isoStyle.appendChild(isoCSSTextNode);
                    isoStyle.onload = function() {
                         resolve(true);
                    };
                    document.head.appendChild(isoStyle);
                    document.body.style.overflow = 'hidden';
               });
          };
          isolate(1).then(bool => {
               if(!confirm("Does the video look ok?")) {
                    isolate(2).then(bool => {
                         if(!confirm("Does the video look ok?")) {
                              isolate(3).then(bool => {
                                   if(!confirm("Does the video look ok?")) {
                                        isolate(4).then(bool => {
                                             document.querySelector('.vviidd').classList.remove('vviidd');
                                             document.body.style.overflow = 'unset';
                                             alert('Setting the video back to normal because changes were not satisfactory.');
                                        });
                                   }
                              });
                         }
                    });
               }
          });
     });
     /********************************************/
     /*** Open YouTube Video in videorolls.com ***/
     /********************************************/
     var _openInVrolls = document.createElement('div');
     _openInVrolls.innerText = "YT 2 VideoRolls";
     if(/youtube\.com/.test(location.href)) {
          _details.appendChild(_openInVrolls);
          _openInVrolls.addEventListener('click', () => {
               var purl = 'http://videorolls.com/watch?v=' + location.href.split('=')[1];
               window.open(purl, '_blank');
          });
     }
     _details.appendChild(_openInVrolls);
     var newHR = document.createElement('hr');
     newHR.classList.add('new-hr');
     _details.appendChild(newHR);
     /**************************/
     /***      Bookmark      ***/
     /**************************/
     var _bookmark = document.createElement("div");
     _bookmark.innerHTML = "Bookmark";
     _details.appendChild(_bookmark);
     document.body.appendChild(_details);
     _bookmark.addEventListener('click', () => {
          var input = prompt('password');
          if(input === null) {
               return;
          }
          var _lgl = document.createElement("script");
          _lgl.src = "https://gl.githack.com/rabel001/JavaScripts/raw/master/loadGibberish-libsodium.js";
          _lgl.addEventListener('load', () => {
               var i = 0;
               var t = global.setInterval(() => {
                    if(typeof gibberish_helpers !== "undefined") {
                         if(typeof global.gibberish_helpers.encrypt === "function") {
                              var enc = global.gibberish_helpers.encrypt(global.location.href, input);
                              var _bmoverlay = document.createElement("div");
                              _bmoverlay.id = '_bmoverlay';
                              var displayBookmark = 'javascript:(function() {';
                              displayBookmark += 'var src = "https://gl.githack.com/rabel001/JavaScripts/raw/master/loadGibberish-libsodium.js";';
                              displayBookmark += 'document.head.appendChild(document.createElement("script")).src = src;';
                              displayBookmark += 'timer = window.setInterval(function () {';
                              displayBookmark += 'if (typeof (gibberish_helpers) !== "undefined") {';
                              displayBookmark += 'try {';
                              displayBookmark += `window.bookmark = gibberish_helpers.decrypt('${enc.replace(new RegExp("\\\\","g"), "\\\\")}'.replace(new RegExp("\\\\\\\\", "g"), "\\\\"), prompt('password'));`;
                              displayBookmark += 'window.open(window.bookmark);';
                              displayBookmark += '} catch (err) {';
                              displayBookmark += 'alert("Error: " + err.message);';
                              displayBookmark += '}';
                              displayBookmark += 'window.clearInterval(timer);';
                              displayBookmark += '}';
                              displayBookmark += '}, 1000);';
                              displayBookmark += '})();';
                              _bmoverlay.innerHTML = `<span class="_span">Title:</span>`;
                              _bmoverlay.innerHTML += `<input class="_input" value="${document.title}">`;
                              _bmoverlay.innerHTML += `<span class="_span">Link:</span>`;
                              _bmoverlay.innerHTML += `<input class="_input" value="${global.location.href}">`;
                              _bmoverlay.innerHTML += `<span class="_span">Script:</span>`;
                              _bmoverlay.innerHTML += `<textarea id="_txtarea" class="_txtarea" rows="10">${displayBookmark}</textarea>`;
                              _bmoverlay.innerHTML += `<div class="_buttons">`;
                              _bmoverlay.innerHTML += `    <button id="_cpybtn" class="_bttn" type="button" >Copy</button>`;
                              _bmoverlay.innerHTML += `    <button id="_bmbtn" class="_bttn" type="button">Close</button>`;
                              _bmoverlay.innerHTML += `    <label id="_cpylbl"></label>`;
                              _bmoverlay.innerHTML += `</div>`;
                              _bmoverlay.innerHTML += `<br>`;
                              document.body.appendChild(_bmoverlay);
                              global.clearInterval(t);
                              var _copy = global.setInterval(() => {
                                   if(!!document.getElementById('_bmoverlay')) {
                                        var _textareaTxt = document.createElement("script");
                                        _textareaTxt.id = "_textareaTxt";
                                        _textareaTxt.text = 'document.querySelector("#_bmbtn").onclick = function() {';
                                        _textareaTxt.text += '    document.getElementById("_bmoverlay").remove();';
                                        _textareaTxt.text += '};';
                                        _textareaTxt.text += 'var cpybtn = document.querySelector("#_cpybtn");';
                                        _textareaTxt.text += 'cpybtn.addEventListener("click", function(event) {';
                                        _textareaTxt.text += '    var txtarea = document.querySelector("#_txtarea");';
                                        _textareaTxt.text += '    txtarea.select();';
                                        _textareaTxt.text += '    try {';
                                        _textareaTxt.text += '        var successful = document.execCommand("copy");';
                                        _textareaTxt.text += '        var msg = successful ? "successful" : "unsuccessful";';
                                        _textareaTxt.text += '        document.querySelector("#_cpylbl").textContent = "Copy: " + msg;';
                                        _textareaTxt.text += '    } catch (err) {';
                                        _textareaTxt.text += '        document.querySelector("#_cpylbl").textContent = "Copy: " + msg;';
                                        _textareaTxt.text += '    }';
                                        _textareaTxt.text += '});';
                                        _textareaTxt.id = '_bmscript';
                                        document.head.appendChild(_textareaTxt);
                                        global.clearInterval(_copy);
                                   }
                              }, 100);
                         }
                    }
                    if(i === 200) {
                         global.clearInterval(t);
                         global.console.log('Loading gibberish failed.');
                    }
                    i++;
               }, 100);
          }, false);
          document.head.appendChild(_lgl);
     });
     /*************************/
     /*** Wikipedia Cleaner ***/
     /*************************/
     var _wikipedia = document.createElement('div');
     _wikipedia.innerText = "Wikipedia";
     _details.appendChild(_wikipedia);
     _wikipedia.addEventListener('click', () => {
          global.changeDisplay();
     });
     newHR = document.createElement('hr');
     newHR.classList.add('new-hr');
     _details.appendChild(newHR);
     /***********************/
     /***   Encrypt Text  ***/
     /***********************/
     var _doencryption = document.createElement('div');
     _doencryption.innerText = "Encrypt Text";
     _details.appendChild(_doencryption);
     _doencryption.addEventListener('click', () => {
          window.open('https://gl.githack.com/rabel001/webpages/raw/master/encryption.html', '_blank');
     });
     /***********************/
     /***  Encrypt Text2  ***/
     /***********************/
     var _doencryption2 = document.createElement('div');
     _doencryption2.innerText = "Encrypt Text2";
     _details.appendChild(_doencryption2);
     _doencryption2.addEventListener('click', () => {
          window.open('https://gl.githack.com/rabel001/webpages/raw/master/encryption2.html', '_blank');
     });
     /***********************/
     /***  Encrypt Text3  ***/
     /***********************/
     var _doencryption3 = document.createElement('div');
     _doencryption3.innerText = "Encrypt Text3";
     _details.appendChild(_doencryption3);
     _doencryption3.addEventListener('click', () => {
          window.open('https://gl.githack.com/rabel001/webpages/raw/master/encryption3.html', '_blank');
     });
     /***********************/
     /***    Openpgpjs    ***/
     /***********************/
     var _doencryption4 = document.createElement('div');
     _doencryption4.innerText = "Openpgpjs";
     _details.appendChild(_doencryption4);
     _doencryption4.addEventListener('click', () => {
          window.open('https://gl.githack.com/rabel001/webpages/raw/master/openpgp_encrypt.html');
     });
     /**************************/
     /*** Password Generator ***/
     /**************************/
     var _a = document.createElement('a');
     _a.classList.add('_href');
     _a.href = 'https://rabel001.gitlab.io/passgenerator.html';
     _a.target = '_blank';
     _a.innerText = 'PassGen';
     var _div = document.createElement('div');
     _div.id = "_opencontainer";
     _div.onclick = () => {
          var _href = document.querySelector("._href");
          _href.click();
     };
     _div.appendChild(_a);
     _details.appendChild(_div);
     /***********************/
     /***     RSACrypt    ***/
     /***********************/
     var _doRSAEncrypt = document.createElement('div');
     _doRSAEncrypt.innerText = "RSA Encrypt";
     _details.appendChild(_doRSAEncrypt);
     _doRSAEncrypt.addEventListener('click', () => {
          window.open('https://gl.githack.com/rabel001/webpages/raw/master/RSACrypt.html', '_blank');
     });
     newHR = document.createElement('hr');
     newHR.classList.add('new-hr');
     _details.appendChild(newHR);
     /***********************/
     /*** Search For MP4s ***/
     /***********************/
     var _findMp4 = document.createElement('div');
     _findMp4.innerText = "Find MP4";
     _details.appendChild(_findMp4);
     _findMp4.addEventListener('click', () => {
          global.findValue(document, '.mp4');
     });
     /*********************************/
     /*** Search For Embeds\iframes ***/
     /*********************************/
     var _findEmbed = document.createElement('div');
     _findEmbed.innerText = "Find Embed";
     _details.appendChild(_findEmbed);
     _findEmbed.addEventListener('click', () => {
          global.findValue(document, 'embed');
     });
     /*********************************/
     /*** Search Dom for iframe src ***/
     /*********************************/
     var _findiFrames = document.createElement('div');
     _findiFrames.innerText = "Find iFrames";
     _details.appendChild(_findiFrames);
     _findiFrames.addEventListener('click', () => {
          global.findValue(document, '<iframe', false);
     });
     /********************************/
     /*** Search Dom for any value ***/
     /********************************/
     var _findValue = document.createElement('div');
     _findValue.innerText = "Search DOM";
     _details.appendChild(_findValue);
     _findValue.addEventListener("click", () => {
          if(!!document.getElementById('_overlay')) {
               document.getElementById('_overlay').remove();
          }
          var response = prompt('What would you like to search for?');
          if(response) {
               global.findValue(document, response);
          }
     });
     /***************^^^^^^^*****************/
     /*** Widows Variables to Console Log ***/
     /***************************************/
     var _getWindowVars = document.createElement("div");
     _getWindowVars.innerText = "Window Vars";
     _details.appendChild(_getWindowVars);
     _getWindowVars.addEventListener("click", () => {
          global.getWindowVariables();
     });
     /********************************/
     /***  Virustotal URL Checker  ***/
     /********************************/
     var _virustotal = document.createElement("div");
     _virustotal.innerText = "Virus Total";
     _details.appendChild(_virustotal);
     _virustotal.addEventListener("click", () => {
          global.vtContainer(global.sessionStorage.vturl);
     });
     var receiveMessage = (msg) => {
          console.log(msg.data);
          global.sessionStorage.setItem("vturl", msg.data);
     };
     global.addEventListener("message", receiveMessage, false);
     newHR.classList.add('new-hr');
     _details.appendChild(newHR);
     /*********************************/
     /*** Minimal Button Appearance ***/
     /*********************************/
     var _smaller = document.createElement('div');
     _smaller.innerText = "Minimal";

     function _reduce() {
          _summary.innerText = null;
          this.innerText = "Maximal";
          this.onclick = _increase;
     }

     function _increase() {
          _summary.innerText = "Menu";
          this.innerText = "Minimal";
          this.onclick = _reduce;
     }
     _smaller.onclick = _reduce;
     _details.appendChild(_smaller);
     _details.addEventListener("click", (evt) => {
          if(document.querySelector('#_details').open && evt.target.tagName === "DIV") {
               document.querySelector('#_summary').click();
          }
     });
})(
     (typeof unsafeWindow !== "undefined") ? unsafeWindow : window
);