// ==UserScript==
// @name            Find Values in DOM
// @author          rabel001
// @homepage        https://rabel001.gitlab.io
// @version         2.1.14.19
// @description     Find any value in the DOM and displays them in an textarea overlay.
// @namespace       https://gitlab.com/rabel001/userscripts/blob/master/findValueInDom.user.js
// @downloadURL     https://glcdn.githack.com/rabel001/userscripts/raw/master/findValueInDom.user.js
// @updateURL       https://glcdn.githack.com/rabel001/userscripts/raw/master/findValueInDom.user.js
// @include         *
// @noframes
// @grant           none
// ==/UserScript==

;((global) => {
     "use strict";

     var getValues = (element, value, innertext = false) => {
          return new Promise((resolve) => {
               var regex = new RegExp('[' + global.fvOptions.specials.join('\\') + ']', 'g');
               var escapedValue = value.replace(regex, "\\$&");
               regex = new RegExp(escapedValue);

               if(innertext) {
                    if(element.nodeName === '#document') {
                         var scripts = Array.from(element.scripts);
                         //for(let i = 0; i <= scripts.length; i++) {
                         //	let s = scripts[i];
                         scripts.forEach((s) => {
                              //if(!!s.innerText) {
                              if(s.innerText.match(escapedValue)) {

                              }
                              //}
                         });
                    }
               }

               if(!(!!element)) {
                    return;
               }
               var allElements = element.querySelectorAll('*');
               for(let i = 0; i < allElements.length; i++) {
                    let e = allElements[i];
                    //allElements.forEach((e) => {
                    var entry = {};
                    if(value === '<iframe' && e.tagName === "IFRAME") {
                         if(e.src) {
                              global.fvOptions.text1 = (!!global.fvOptions.text1) ? global.fvOptions.text1 + e.src + '\n' : e.src + '\n';
                              entry.src = e.src;
                              global.fvOptions.values.push(entry);
                         }
                    } else {
                         if(e.tagName === "IFRAME") {
                              try {
                                   if(!!e.contentDocument) {
                                        global.fvOptions.iframeErrors = false;
                                   }
                              } catch (ex) {
                                   global.fvOptions.iframeErrors = true;
                              }
                              if(!global.fvOptions.iframeErrors) {
                                   getValues(e.contentDocument, value);
                              } else {
                                   global.fvOptions.iframesWithErrors.push(e);
                              }

                         }
                    }
                    if(e.hasAttributes()) {
                         for(let prop in e.attributes) {
                              if(e.attributes.hasOwnProperty(prop)) {
                                   if(regex.test(e.attributes[prop].value)) {
                                        entry[e.attributes[prop].name] = e.attributes[prop].value;
                                        global.fvOptions.values.push(entry);
                                        global.fvOptions.text1 = (!!global.fvOptions.text1) ? global.fvOptions.text1 + global.fvOptions.Attribute + e.attributes[prop].value + '\n' : global.fvOptions.Attribute + e.attributes[prop].value + '\n';
                                   }
                              }
                         }
                    }
                    try {
                         var m = e.innerText.match(/("(.*?)"|'(.*?)')/g);
                         if(m) {
                              for(let n = 0; i < m.length; n++) {
                                   let r = m[n];
                                   //m.forEach((r) => {
                                   if(r.match(escapedValue)) {
                                        r = r.replace(/['"]/g, '');
                                        global.fvOptions.innerQuotesMatches.push(r);
                                        entry.innerText = r;
                                        global.fvOptions.values.push(entry);
                                        global.fvOptions.text1 = (!!global.fvOptions.text1) ? global.fvOptions.text1 + global.fvOptions.Quote + r + '\n' : global.fvOptions.Quote + r + '\n';
                                   }
                              } //);
                         }
                         if(e.tagName !== "head") {
                              if(innertext) {
                                   if(regex.test(e.innerText)) {
                                        global.fvOptions.innerTextMatches.push(e.innerText);
                                        entry.innerText = e.innerText;
                                        global.fvOptions.values.push(entry);
                                        global.fvOptions.text2 = (!!global.fvOptions.text2) ? global.fvOptions.text2 + global.fvOptions.InsideText + e.innerText + '\n' : global.fvOptions.InsideText + e.innerText + '\n';
                                   }
                              }
                         }
                    } catch (ex) {}
               } //);

               if(!!global.fvOptions.text1 && !!global.fvOptions.text2) {
                    global.fvOptions.text = global.fvOptions.text1.concat(global.fvOptions.text2);
               }
               if(!!global.fvOptions.text1 && !(!!global.fvOptions.text2)) {
                    global.fvOptions.text = global.fvOptions.text1;
               }
               if(!(!!global.fvOptions.text1) && !!global.fvOptions.text2) {
                    global.fvOptions.text = global.fvOptions.text2;
               }
               if(!(!!global.fvOptions.text1) && !(!!global.fvOptions.text2)) {
                    getValues(element, value, true);
               }

               //global.fvOptions.text = global.fvOptions.text1;
               global.fvOptions.exports.text = global.fvOptions.text;
               if(global.fvOptions.iframesWithErrors.length > 0) global.fvOptions.exports.iframesWithErrors = global.fvOptions.iframesWithErrors;
               if(global.fvOptions.iframeErrors) global.fvOptions.exports.iframeErrors = global.fvOptions.iframeErrors;
               if(global.fvOptions.values.length > 0) global.fvOptions.exports.values = global.fvOptions.values;
               if(global.fvOptions.innerTextMatches.length > 0) global.fvOptions.exports.innerTextMatches = global.fvOptions.innerTextMatches;
               if(global.fvOptions.innerQuotesMatches.length > 0) global.fvOptions.exports.innerQuotesMatches = global.fvOptions.innerQuotesMatches;
               resolve(global.fvOptions.exports);
               //resolve(global.fvOptions.text);
          });
     };
     var findValue = (element, value) => {
          var dashes = '--------------------------------------------------------------------';
          global.fvOptions = {
               specials: ["-", "[", "]", "/", "{", "}", "(", ")", "*", "+", "?", ".", "\\", "^", "$", "|"],
               vidExtensions: /(\.webm|\.mkv|\.flv|\.mp4|\.f4v|\.mpg|\.mpeg|\.wav|\.wmv|\.avi|\.mov|\.ogg)/,
               text: null,
               text1: null,
               text2: null,
               values: [],
               iframeErrors: false,
               innerTextMatches: [],
               iframesWithErrors: [],
               innerQuotesMatches: [],
               exports: {},
               Quote: '\n' + dashes + 'Quote' + dashes + '\n',
               Attribute: '\n' + dashes + 'Source' + dashes + '\n',
               InsideText: '\n' + dashes + 'Inner Text' + dashes + '\n'
          };
          if(!!document.getElementById('_overlay')) {
               document.getElementById('_overlay').remove();
          }
          getValues(element, value).then((imports) => {
               var cssText = `#_overlay,#_ul{height:100%!important}#_ul,._li{list-style:decimal inside!important}#_exit,#_openLink,#_text{position:absolute!important}#_overlay{position:fixed!important;display:none;width:100%!important;top:0!important;left:0!important;right:0!important;bottom:0!important;background-color:rgba(0,0,0,.5)!important;z-index:200000!important}#_text{top:50%!important;left:50%!important;height:80%!important;width:80%!important;z-index:1!important;font-size:14px!important;font-family:Arial!important;color:#000!important;background-color:#fff!important;transform:translate(-50%,-50%)!important;-ms-transform:translate(-50%,-50%)!important}#_exit{top:5px!important;right:5px!important;z-index:20000000!important;font-size:25px!important;font-weight:700!important;color:#fff!important;cursor:pointer!important}#_ul{overflow:auto!important;text-align:left!important;margin-left:20px!important;padding:2px!important}#_openLink,._openLink{font-size:14px!important;font-family:Arial!important;line-height:14px!important;height:30px!important;padding:5px!important;background-color:#444!important;color:#fff!important}#_openLink{top:11px!important;right:30px!important;border:0!important;border-radius:3px!important}._openLink{margin:2px!important;border:0!important;border-radius:3px!important}.clickedlink,.finddomlink:link{text-decoration:underline!important;font-size:13px!important}.finddomlink:link{color:#00f!important}.clickedlink,.finddomlink:visited{color:purple!important}`;
               var cssTextNode = document.createTextNode(cssText);
               var css = document.createElement('style');
               css.appendChild(cssTextNode);
               css.id = 'findValues';
               document.head.appendChild(css);
               var _overlay = document.createElement('div');
               _overlay.id = '_overlay';
               //var _textarea = document.createElement('textarea');
               var _textarea = document.createElement('div');
               _textarea.id = '_text';
               _textarea.name = 'text';
               //if(imports.text == null) imports.text = 'Nothing found';
               var ul = document.createElement('ul');
               ul.id = '_ul';
               /*
				ol.onclick = function(evt) {
					if(/checkbox/.test(evt.target.id)) {
						var allCheckboxes = document.querySelectorAll('#_text #_ol input[type="checkbox"]');
						allCheckboxes.forEach((cbox) => {
							if(cbox.id !== evt.target.id) {
								cbox.checked = false;
							}
						});
					}
				}
            */
               global.vals = imports;
               for(let i in imports.values) {
                    if(JSON.stringify(imports.values[i]) !== undefined) {
                         var val = JSON.stringify(imports.values[i]).split('":"')[1].replace(/['"]/m, '').replace('}', '');
                    }
                    //global.val = imports.values[i];
                    var li = document.createElement('li');
                    li.classList.add('_li');
                    /*
					var openLink = document.createElement('input');
					openLink.type = 'submit';
					openLink.id = '_openLink2';
					openLink.value = 'Open';
					openLink.onclick = function() {
						//console.log(this.nextElementSibling);
						global.open(this.nextElementSibling.innerText, '_blank');
					}
					openLink.classList.add('_openLink');
					var checkbox = document.createElement('input');
					checkbox.type = 'checkbox';
					checkbox.id = 'checkbox_' + i;
                */
                    var a = document.createElement('a');
                    a.id = '_a' + i;
                    a.innerText = val;
                    a.href = val;
                    a.target = '_blank';
                    a.classList.add('finddomlink');
                    a.onclick = function() {
                         this.classList.remove('finddomlink');
                         this.classList.add('clickedlink');
                    };
                    //li.appendChild(checkbox);
                    //li.appendChild(openLink);
                    li.appendChild(document.createTextNode(' '));
                    li.appendChild(a);
                    ul.appendChild(li);
               }
               /*
               	var btn = document.createElement('input');
               	btn.id = '_openLink';
               	btn.type = 'submit';
               	btn.value = 'Open';
               	btn.onclick = function() {
               		document.querySelectorAll('#_ol > li').forEach((e) => {
               			if(e.firstElementChild.checked) {
               				global.open(e.lastElementChild.innerText, '_blank');
               			}
               		});
               	}
               	//_textarea.value = imports.text;
               	ol.appendChild(btn);
               */
               _textarea.appendChild(ul);
               _overlay.appendChild(_textarea);
               _overlay.style.display = 'block';
               var span = document.createElement('span');
               span.id = '_exit';
               span.innerHTML = '&#x2717;';
               span.addEventListener('click', () => {
                    document.getElementById('_overlay').style.display = 'none';
               });
               _overlay.appendChild(span);
               _overlay.onclick = (evt) => {
                    if(evt.target.id === '_overlay') {
                         //if(evt.srcElement.tagName === 'DIV') {
                         document.getElementById('_exit').click();
                    }
               };
               document.body.appendChild(_overlay);
               global.fvOptions = null;
          });
     };
     var getWindowVariables = () => {
          console.log("======================= Window Variables ======================= ");
          for(let item in window) {
               if(window.hasOwnProperty(item)) {
                    if(typeof window[item] !== "function" && window[item] !== null) {
                         console.log({
                              [item]: window[item]
                         });
                    }
               }
          }
          console.log("======================= Window Variables ======================= ");
     }
     global.findValue = findValue;
     global.getWindowVariables = getWindowVariables;
})(window);