// ==UserScript==
// @name            _Scriptable
// @author          rabel001
// @homepage        https://gitlab.com/rabel001/userscripts
// @version         1.00
// @description     Add scripts to current page using _Scriptable.add('<script>.js') or _Scriptable.add(_Scriptable.available_Scripts.<scriptname>)
// @namespace       https://gitlab.com/rabel001/userscripts/blob/master/_Scriptable.js
// @updateURL       https://glcdn.githack.com/rabel001/userscripts/raw/master/_Scriptable.js
// @downloadURL     https://glcdn.githack.com/rabel001/userscripts/raw/master/_Scriptable.js 
// @include         *
// @include         about:blank
// @grant           none
// ==/UserScript==

((global, doc) => {
	'use strict';

	var _scriptable = {
		add: async (script) => {
			var help;
			if(typeof script == "object") {
				if(!!script.script) {
					help = script.help;
					script = script.script;
				}
			}
			var promise = (nscript) => {
				return new Promise((resolve, reject) => {
					var failed = false;
					try {
						var newScript = doc.createElement('script');
						newScript.type = 'text/javascript';
						newScript.className = 'newScript';
						newScript.src = nscript;
						newScript.onerror = function(event) {
							failed = event.type;
						}
						newScript.type = 'text/javascript';
						doc.head.appendChild(newScript);
						var i = 0;
						var cycle = setInterval(() => {
							setTimeout(() => {
								if(!!doc.getElementsByClassName('newScript') && !failed) {
									clearInterval(cycle);
									resolve(`_Script output:\n\t- Loaded: true\n\t- Script Element: ${newScript.outerHTML}`);
								}
								else {
									clearInterval(cycle);
									reject('_Script Error:\n\t- Script failed to load with error code "' + failed + '".\n\t- `Script Name: ' + nscript);
								}
							}, 98);
							i++;
						}, 100);
						if(i === 25) {
							reject('_Script:\n\t-Interval took to long.');
							clearInterval(cycle);
						}
					}
					catch(err) {
						//console.log('Error loading ' + nscript.substring(nscript.lastIndexOf('/') + 1, nscript.length));
						//console.log('Error message: ' + err.message);
						var err0 = 'Script Output:\n\t- Error loading ' + nscript.substring(nscript.lastIndexOf('/') + 1, nscript.length) + '\n\t- Error message: ' + err.message
						reject(err0);
					}
				});
			}
			promise(script).then((data) => {
				console.log(data);
				if(help !== undefined) {
					console.log('\t- Script Help: ' + help);
				}
			}).catch((error) => {
				console.log(error);
			});
		},
		available_Scripts: {
			html5shiv: {
				script: 'https://rawgit.com/aFarkas/html5shiv/master/src/html5shiv.js',
				help: 'https://github.com/aFarkas/html5shiv'
			},
			jquery1113: {
				script: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.js',
				help: 'https://jquery.com'
			},
			jquery224: {
				script: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.js',
				help: 'https://jquery.com'
			},
			jquery300: {
				script: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.js',
				help: 'https://jquery.com'
			},
			underscore: {
				script: 'https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js',
				help: 'http://underscorejs.org'
			},
			angular: {
				script: 'https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular.min.js',
				help: 'https://github.com/angular/angular.js'
			},
			platform: {
				script: 'https://rawgit.com/bestiejs/platform.js/master/platform.js',
				help: 'https://github.com/bestiejs/platform.js'
			},
			bowser: {
				script: 'https://rawgit.com/ded/bowser/master/src/bowser.js',
				help: 'https://github.com/ded/bowser'
			},
			arrive: {
				script: 'https://rawgit.com/uzairfarooq/arrive/master/src/arrive.js',
				help: 'https://github.com/uzairfarooq/arrive'
			},
			zest: {
				script: 'https://rawgit.com/chjj/zest/master/lib/zest.js',
				help: 'https://github.com/chjj/zest'
			},
			HTML: {
				script: 'https://rawgit.com/nbubna/HTML/master/dist/HTML.js',
				help: 'http://nbubna.github.io/HTML'
			},
			head: {
				script: 'https://cdnjs.cloudflare.com/ajax/libs/headjs/1.0.3/head.min.js',
				help: 'http://headjs.com'
			},
			tags: {
				script: 'https://rawgit.com/a-tarasyuk/tag/master/lib/tag.js',
				help: 'https://github.com/a-tarasyuk/tag'
			},
			sugar: {
				script: 'https://rawgit.com/andrewplummer/Sugar/master/dist/sugar.js',
				help: 'https://sugarjs.com'
			},
			requirejs: {
				script: 'https://requirejs.org/docs/release/2.3.5/comments/require.js',
				help: 'https://requirejs.org'
			},
			detectBrowser: {
				script: 'https://rawgit.com/darcyclarke/Detect.js/master/detect.js',
				help: 'https://github.com/darcyclarke/Detect.js'
			},
			is: {
				script: 'https://rawgit.com/arasatasaygin/is.js/master/is.js',
				help: 'http://is.js.org'
			},
			xregexp: {
				script: 'https://rawgit.com/slevithan/xregexp/master/xregexp-all.js',
				help: 'https://github.com/slevithan/xregexp/blob/master/README.md'
			},
			respond: {
				script: 'https://oss.maxcdn.com/respond/1.4.2/respond.min.js',
				help: 'https://github.com/scottjehl/Respond'
			},
			dataTables: {
				script: 'https//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js',
				help: 'https://www.datatables.net'
			}
		}
	};
	global._Scriptable = _scriptable;
})(window, document);