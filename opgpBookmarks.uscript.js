// ==UserScript==
// @name            Openpgp Bookmarks
// @author          rabel001
// @homepage        https://gitlab.com/rabel001/userscripts
// @version         1.00
// @namespace       https://gitlab.com/rabel001/userscripts/blob/master/opgpBookmarks.uscript.js
// @updateURL       https://glcdn.githack.com/rabel001/userscripts/raw/master/opgpBookmarks.uscript.js
// @downloadURL     https://glcdn.githack.com/rabel001/userscripts/raw/master/opgpBookmarks.uscript.js
// @include         *
// @exclude         *spotify.com*
// @require         https://unpkg.com/dexie@latest/dist/dexie.js
// @grant           none
// ==/UserScript==

//
//This only works in non-incognito mode.
//

(() => {
	var TITLE = 'Openpgp Bookmarks';
	//window.open('http://www.blank.org', '_blank');
	var openpgpBookmark = {
		isCorrectPass: null,
		addBkmkBtn: () => {
			var cssText = `
			#tblbkmk {
				position: fixed !important;
				bottom: 0 !important;
				right: 0 !important;
				z-index: 2147483646 !important;
				opacity: 0.5 !important;
				background-color: black !important;
				color: white !important;
				text-align: center !important;
				font-size: 14px !important;
				font-family: arial, sans-serif !important;
				line-height: 18px !important;
			}
			#tblheader {
				cursor: move !important;
				z-index: 10 !important;
				width: 25px !important;
				background-color: white !important;
				border: 0;
				padding: 0;
				margin: 0;
				color: white !important;
			}
			#tblheader img {
				vertical-align: middle !important;
				text-align: center !important;
				width: 25px !important;
				height: 26px !important;
			}
			#bkmkbtn {
				padding: 5px !important;
			}
			`
			var tbl = document.createElement('table');
			tbl.id = 'tblbkmk';
			tbl.innerHTML = `
			<tbody>
			<tr>
				<td id="tblheader"><img src="https://cdn4.iconfinder.com/data/icons/cursors-gestures/100/cursor_3-128.png" alt=""></td>
				<td id="bkmkbtn">Bookmark!</td>
			</tr>
			</tbody>
			`
			var style = document.createElement('style');
			style.type = 'text/css';
			var cssTextNode = document.createTextNode(cssText);
			style.appendChild(cssTextNode);
			style.id = 'bkmkStyle';
			document.head.appendChild(style);
			document.body.appendChild(tbl);
			var script = document.createElement('script');
			script.text = `
				function dragElement(elmnt) {
					var pos1 = 0,
						pos2 = 0,
						pos3 = 0,
						pos4 = 0;
					if(document.getElementById(elmnt.id + "header")) {
						/* if present, the header is where you move the DIV from:*/
						document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
					}
					else {
						/* otherwise, move the DIV from anywhere inside the DIV:*/
						elmnt.onmousedown = dragMouseDown;
					}
					function dragMouseDown(e) {
						e = e || window.event;
						// get the mouse cursor position at startup:
						pos3 = e.clientX;
						pos4 = e.clientY;
						document.onmouseup = closeDragElement;
						// call a function whenever the cursor moves:
						document.onmousemove = elementDrag;
					}
					function elementDrag(e) {
						e = e || window.event;
						// calculate the new cursor position:
						pos1 = pos3 - e.clientX;
						pos2 = pos4 - e.clientY;
						pos3 = e.clientX;
						pos4 = e.clientY;
						// set the element's new position:
						elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
						elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
					}
					function closeDragElement() {
						/* stop moving when mouse button is released:*/
						document.onmouseup = null;
						document.onmousemove = null;
					}
				}
				dragElement(document.getElementById("tblbkmk"));
			`
			document.head.appendChild(script);
			document.getElementById('bkmkbtn').addEventListener('click', openpgpBookmark.getPassword);
		},
		createBkmkTable: () => {
			var cssText = `
			#bookmarklist {
				width: 100%;
				border-collapse: collapse;
			}
			th,
			td {
				padding: 5px;
			}
			tr:hover {
				background-color: lightgray !important;
			}
			th:nth-child(1) {
				border-right-color: white;
			}
			th:nth-child(2),
			td:nth-child(2) {
				text-align: center;
				width: 30%;
			}
			tr,
			th,
			td {
				border: 1px solid black;
			}
			td {
				padding-left: 15px;
			}
			.even-row {
				background-color: #EDEDED !important;
			}
			.odd-row {
				background-color: #ffffff !important;
			}
			th {
				/* background-color: lightblue; */
				background-color: black;
				font-size: 95% !important;
				color: white;
			}
			td {
				font-size: 85% !important;
			}
			`;
			var cssTextNode = document.createTextNode(cssText);
			var style = document.createElement('style');
			style.appendChild(cssTextNode);
			style.id = 'table-css';
			document.head.appendChild(style);
			document.getElementById('p-input').style.display = 'none';
			document.getElementById('p-btn').style.display = 'none';
			var div = document.createElement('div');
			div.id = 'd-bookmarklist';
			div.innerHTML = `
			<table id="bookmarklist" id="display responsive nowrap">
				<thead>
					<tr>
						<th>Encrypted Bookmarks</th>
						<th>Delete</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Empty</td>
						<td>Empty</td>
					</tr>
					<tr>
						<td>Empty</td>
						<td>Empty</td>
					</tr>
					<tr>
						<td>Empty</td>
						<td>Empty</td>
					</tr>
				</tbody>
			</table>
			`
			div.onload = function() { window.console.log('done'); }
			document.getElementById('content').appendChild(div);
			var trs = Array.from(document.getElementsByTagName('tr'));
			var i = 0;
			trs.forEach((tr) => {
				if(i % 2 == 0) {
					tr.classList.add('even-row');
				}
				if(i % 2 == 1) {
					tr.classList.add('odd.row');
				}
				i++
			});
		},
		getPassword: () => {
			var options = 'menubar=no,toolbar=no,location=no,directories=no,status=no,scrollbars=no,resizable=no,dependent,width=100,height=100,left=0,top=0';
			WinId = window.open('', 'newwin', options);
			if(!WinId.opener) WinId.opener = self;
			var Text = '<form style="width: 100%; margin: auto;" ';
			Text += 'onSubmit="opener.openpgpBookmark.testPwd2(this.password.value); self.close()">';
			Text += '<label style="width: 80%; margin: auto; margin-top: 20px; display: block;">Password:</label>';
			Text += '<input type="password" name="password" style="width: 80%; margin: auto; display: block;">';
			Text += '<span style="width: 80%; margin: auto; display: block;"><input style="margin-top: 5px;" type="submit" value="Submit"></span>';
			Text += '<\/form>';
			WinId.document.open();
			WinId.document.write(Text);
			WinId.document.title = TITLE;
			WinId.document.close();
		},
		getKeys: () => {
			return new Promise((resolve, reject) => {
				var openpgpkeys = {
					finished: false,
					gotKeys: 0,
					keyPair: []
				};
				var db = window.openDatabase('bookmarkdb', '1.0', 'bookmarks', 3 * 1024 * 1024 * 1024);
				db.transaction(function(tx) {
					tx.executeSql('SELECT pub, priv FROM keys', [], function(tx, results) {
						openpgpkeys.keyPair.push({ pub: results.rows.item(0).pub, priv: results.rows.item(0).priv });
						if(results.rows.length > 0) {
							openpgpkeys.gotKeys = 1;
							openpgpkeys.finished = true;
							resolve(openpgpkeys);
						}
						else {
							reject(openpgpkeys);
						}
					}, function(transaction, error) {
						console.log(error.message);
						reject(openpgpkeys);
					});
				});
			});
		},
		getDBKeys: async () => {
			return new Promise((resolve, reject) => {
				openpgpBookmark.getKeys().then((keyPair) => {
					resolve(keyPair);
				}).catch((err) => {
					reject(err);
				});
			});
		},
		testPwd2: async (pwd) => {
			console.log(pwd);
			openpgpBookmark.getDBKeys().then((keyPair) => {
				window.okeys = keyPair.keyPair[0];
				var oPrivKey = openpgp.key.readArmored(atob(keyPair.keyPair[0].priv)).keys[0];
				oPrivKey.decrypt(pwd).then(function(bool) {
					openpgpBookmark.isCorrectPass = bool;
					alert('Password is correct.');
				}).catch(function(err) {
					openpgpBookmark.isCorrectPass = false;
					alert('Password is incorrect.');
				});
			}).catch((error) => {
				alert('ERROR!');
			});
			/*window.testpwdtimer = setInterval(function() {
				openpgpBookmark.showBookmarks(openpgpBookmark.isCorrectPass, keyPair.keyPair[0]);
				clearInterval(window.testpwdtimer);
			}, 100);*/
		},
		testPwd: async () => {
			var pwd = document.getElementById("new-password").value;
			var keyPair = await openpgpBookmark.getDBKeys();
			window.okeys = keyPair.keyPair[0];
			var oPrivKey = openpgp.key.readArmored(atob(keyPair.keyPair[0].priv)).keys[0];
			oPrivKey.decrypt(pwd).then(function(bool) {
				openpgpBookmark.isCorrectPass = bool;
			}).catch(function(err) {
				openpgpBookmark.isCorrectPass = false;
			});
			window.testpwdtimer = setInterval(function() {
				openpgpBookmark.showBookmarks(openpgpBookmark.isCorrectPass, keyPair.keyPair[0]);
				clearInterval(window.testpwdtimer);
			}, 100);
		},
		submitPass: () => {
			var newPassword = document.getElementById("new-password").value;
			var newPasswordRepeat = document.getElementById("new-password-repeat").value;
			if(newPassword === newPasswordRepeat && (newPassword !== '')) {
				//alert('true');
				document.getElementById("setup-account").innerHTML = "Generating keys";
				document.getElementById("setup-account").disabled = true;
				var options = {
					userIds: [{
						name: 'Encrypted Bookmarks User',
						email: 'example@example.com'
					}],
					numBits: 2048,
					passphrase: newPassword
				};
				//var privkey = null;
				//var pubkey = null;
				openpgp.generateKey(options).then(function(key) {
					openpgpBookmark.keyPair = key;
					var db = window.openDatabase('bookmarkdb', '1.0', 'bookmarks', 3 * 1024 * 1024 * 1024);
					db.transaction(function(tx) {
						tx.executeSql('CREATE TABLE IF NOT EXISTS keys (id INTEGER PRIMARY KEY AUTOINCREMENT, pub TEXT, priv TEXT)');
					});
					var blobpub = btoa(key.publicKeyArmored);
					var blobpriv = btoa(key.privateKeyArmored);
					db.transaction(function(tx) {
						tx.executeSql('INSERT INTO keys (pub, priv) VALUES ("' + blobpub + '", "' + blobpriv + '")');
					});
					location.reload();
				});
			}
			else {
				alert('false');
			}
		},
		encryptBookmark: function(title) {
			return new Promise(resolve => {
				var url = window.location.href;
				var keys = openpgpBookmark.getDBKeys();
				var arr = [];
				if(keys.gotKeys) {
					var optionsTitle = {
						data: title,
						publicKeys: openpgp.key.readArmored(atob(keys.keyPair[0].pub)).keys
					};
					openpgp.encrypt(optionsTitle).then(function(ciphertextTitle) {
						var optionsUrl = {
							data: url,
							publicKeys: openpgp.key.readArmored(atob(keys.keyPair[0].pub)).keys
						};
						openpgp.encrypt(optionsUrl).then(function(ciphertextUrl) {
							arr.push({
								"title": ciphertextTitle.data,
								"url": ciphertextUrl.data
							});
							resolve(arr);
						});
					});
				}
			});
		},
		addBookmarkToDB: async function() {
			var response = prompt("Bookmark title", document.title);
			var ebookmark = await openpgpBookmark.encryptBookmark(response);
			window.addcryptmarktimer = setInterval(function() {
				if(ebookmark[0]) {
					//window.console.log(ebookmark[0]);
					var db = window.openDatabase('bookmarkdb', '1.0', 'bookmarks', 3 * 1024 * 1024 * 1024);
					db.transaction(function(tx) {
						tx.executeSql('CREATE TABLE IF NOT EXISTS bookmarks (id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, url TEXT)');
						tx.executeSql('INSERT INTO bookmarks (title, url) VALUES ("' + btoa(ebookmark[0].title) + '", "' + btoa(ebookmark[0].url) + '")');
					});
					clearInterval(window.addcryptmarktimer);
				}
			}, 100);
		},
		getBookmarks: async () => {
			var crypticbookmarks = [];
			var db = window.openDatabase('bookmarkdb', '1.0', 'bookmarks', 3 * 1024 * 1024 * 1024);
			db.transaction(function(tx) {
				tx.executeSql('SELECT * FROM bookmarks', [], function(tx, results) {
					var len = results.rows.length,
						i;
					for(i = 0; i < len; i++) {
						crypticbookmarks.push(results.rows.item(i));
					}
				});
			});
			return crypticbookmarks;
		},
		decryptBookmarks: async (keyPair) => {
			openpgpBookmark.uncryptbmarks = [];
			//timer1 = setInterval(function() {
			var privKeyObj = openpgp.key.readArmored(atob(keyPair.priv)).keys[0];
			privKeyObj.decrypt(document.getElementById('new-password').value);

			var crypticbookmarks = await openpgpBookmark.getBookmarks();
			openpgpBookmark.crypticbookmarks = crypticbookmarks;

			//timer2 = setInterval(function() {
			if(crypticbookmarks[0]) {
				for(var i = 0; i < crypticbookmarks.length; i++) {
					var p = document.createElement('p');
					var a = document.createElement('a');
					a.target = '_blank';
					optionsTitle = {
						message: openpgp.message.readArmored(atob(crypticbookmarks[0].title)),
						privateKey: privKeyObj
					};
					openpgp.decrypt(optionsTitle).then(function(plaintextTitle) {
						a.innerText = plaintextTitle.data;
						openpgpBookmark.uncryptbmarks.push({
							"title": plaintextTitle.data
						});
					});
					optionsUrl = {
						message: openpgp.message.readArmored(atob(crypticbookmarks[0].url)),
						privateKey: privKeyObj
					};
					openpgp.decrypt(optionsUrl).then(function(plaintextUrl) {
						a.href = plaintextUrl.data;
						openpgpBookmark.uncryptbmarks.push({
							"url": plaintextUrl.data
						});
					});
					p.appendChild(a);
					document.getElementById('p-div').appendChild(p);
					document.getElementById('p-input').remove();
					document.getElementById('p-btn').remove();
					document.getElementById('p-error').remove();
				}
				//clearInterval(timer2);
			}
			else {
				openpgpBookmark.createBkmkTable();
			}
			//}, 100);
			//clearInterval(timer1);
			//}, 100);
		},
		setupPage: (keyPair = null) => {
			//var keyPair = openpgpBookmark.getDBKeys();
			var cssText = `html, body {
				width: 100%;
				background-color: white;
				color: black;
				margin: 0;
				padding: 0;
				font-family: "Segoe UI", Tahoma, sans-serif;
			}
			#content {
				width: 80%;
				margin: auto;
			}
			#p1 {
				margin-top: 30px;
				font-weight: bold;
				border-bottom: 1px solid black;
				padding: 4px;
			}
			#bookmarks {
				height: 75%;
			}`;
			var cssTextNode = document.createTextNode(cssText);
			var style = document.createElement('style');
			style.appendChild(cssTextNode);
			style.id = 'pgp-css';
			document.body.innerHTML = null;
			//document.getElementById('_css').remove();
			while(document.body.attributes.length > 0) {
				document.body.removeAttribute(document.body.attributes[0].name);
			}
			document.head.appendChild(style);
			document.title = TITLE;
			var script = document.createElement('script');
			script.src = 'https://rawgit.com/openpgpjs/openpgpjs/master/dist/openpgp.js';
			//script.src = 'https://cdnjs.cloudflare.com/ajax/libs/openpgp/3.0.7/openpgp.min.js'
			document.head.appendChild(script);
			var div = document.createElement('div');
			div.id = 'content';
			div.onload = function() { console.log('here1'); }
			var innerHTML;
			//window.stupidKeypairs = keyPair;
			//if(true) {
			if(keyPair.gotKeys === 0) {
				//	alert(keyPair.gotKeys);
				innerHTML = `<div id="create-password">
					<h4>Create your encryption password</h4>
					<p>
						Encrypted Bookmarks will generate a public and private key to, respectively, encrypt and decrypt your bookmarks. This password
						will encrypt your private key.
						<br> Please note:
						<p>
							<ul>
								<li>Recovery is impossible if you forget this password.</li>
								<li>This password cannot be changed.</li>
								<li>Make this password long to protect yourself against brute-force attacks.</li>
							</ul>
						</p>
					</p>
					<p style="display: grid;">
						Password:
						<input type="password" id="new-password">
					</p>
					<p style="display: grid;">
						Password (repeat):
						<input type="password" id="new-password-repeat">
					</p>
					<p id="p-btn">
						<button id="setup-account" onclick="openpgpBookmark.submitPass();">Confirm password</button>
					</p>
				</div>`;
				div.innerHTML = `<p id="p1">${TITLE}</p>${innerHTML}`;
			}
			else {
				//var pass = openpgpBookmark.getPassword();
				innerHTML = `<p id="p-input" style="display: grid;">
				Enter your password.<br><br>
				Password:
				<input type="password" id="new-password">
				</p>
				<p id="p-btn">
					<button id="setup-account" onclick="openpgpBookmark.testPwd();">Login</button>
				</p>
				<p id="p-error"></p>`;
				div.innerHTML = `<p id="p1">${TITLE}</p>${innerHTML}<div id="p-div"></div>`;
			}
			document.body.appendChild(div);
		},
		showBookmarks: (bool, keyPair) => {
			if(bool) {
				openpgpBookmark.bookmarks = openpgpBookmark.decryptBookmarks(keyPair);

			}
			else {
				document.getElementById('p-error').innerText = 'Incorrect password.';
			}
		},
		sleep: (milliseconds) => {
			var start = new Date().getTime();
			for(var i = 0; i < 1e7; i++) {
				if((new Date().getTime() - start) > milliseconds) {
					break;
				}
			}
		},
		dosleep: (item) => {
			if(item.length === 0) {
				alert(item.length);
				openpgpBookmark.sleep(1000);
				openpgpBookmark.dosleep(item);
			}
		}
	};
	window.openpgpBookmark = openpgpBookmark;
	var isPassCorrect;
	window.openpgpBookmark.isPassCorrect = isPassCorrect = false;
	var isBLANKPAGE = /blank\.org/.test(location.href);
	if(!isBLANKPAGE) {
		if(window.top == window.self) {
			window.openpgpBookmark.addBkmkBtn();
			return;
		}
	}
	else {
		(async () => {
			openpgpBookmark.getDBKeys().then((keyPair) => {
				//if(!!document.getElementById('_css')) {
				openpgpBookmark.setupPage(keyPair);
				//}
			}).catch((error) => {
				//if(!!document.getElementById('_css')) {
				openpgpBookmark.setupPage(error);
				//}
			});
		})();
	}
})();