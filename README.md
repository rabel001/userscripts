# UserScripts Repo

[Go straight to the UserScripts Repo.](https://gitlab.com/rabel001/userscripts)

------
Repo of userscripts I've created and currently maintain.

## _Scriptable.js
* Adds script to the head element.
* To add javascript file to the head element use the following command:

     ```js
     _Scriptable.add('https://to.your/script.js');
     ```
* Example:
    ```js
    _Scriptable.add('https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js');
    ```
* This script also contains a list of available scripts
   * Use "_Scriptable.available_Scripts" to see a list of available scripts.
   * An example of a script that can be used from this usercript:
   
       ```js
       _Scriptable.available_Scripts.bowser.script
       ```
   * To get the link to a script's creator use the folling:
   
       ```js
       _Scriptable.available_Scripts.bowser.help
       ```
   * Example for adding script from this userscript:
   
       ```js
       _Scriptable.add(_Scriptable.available_Scripts.bowser);
       ```

## _Styleable.js
* Add link element with stylesheet href to the head element.
* To add stylesheet to the head element use the following command:

    ```js
    _Styleable.add('https//to.your/stylesheet.css');
    ```
* Example:
    ```js
    _Styleable.add('https://cdnjs.cloudflare.com/ajax/libs/10up-sanitize.css/4.1.0/sanitize.min.css');
    ```
* This script also contains a list of available scripts
   * Use "_Stylable.available_StyleSheets" to see a list of available scripts.
   * An example of a script that can be used from this usercript:
   
        ```js
        _Stylable.available_StyleSheets.sanitize.stylesheet;
        ```
   * To get the link to a script's creator use the folling:
   
        ```js
        _Stylable.available_StyleSheets.sanitize.help;
        ```
   * Example for adding script from this userscript:
   
        ```js
        _Styleable.add(_Scriptable.available_StyleSheets.sanitize);
        ```
          
## injectScript.js
* Inject scripts to current page.
* To add a javascript file to the head element use the following command:

    ```js
    injectScript('http//to.your/script.js');
    ```
* Example:

     ```js
     injectScript('https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js');
     ```
* This script also contains a list of available scripts

   * You can get a list of available scripts with the following command:    
     ```js
     injectScript.available_Scripts.list();
     ```
     
   * Use "injectScript.available_Scripts" to see a list of available scripts.
   * An example of a script that can be used from this usercript:
   
        ```js
        injectScript.available_Scripts.bowser.script;
        ```
   * To get the link to a script's creator and for more help, use the folling expample:
       
        ```js
        injectScript.available_Scripts.bowser.help.page;
        ```
      * Or to open the help page, use the following example;
      
        ```js
        injectScript.available_Scripts.bowser.help.open();
        ```
   * Available scripts have an inject function to inject the script.
     Here's an example:
       
        ```js
        injectScript.available_Scripts.bowser.inject();
        ```
   * Another way to inject available scripts is as follows:
       
        ```js
        injectScript(injectScript.available_Scripts.bowser);
        ```
          
* You can also inject a string.
   * Example:
   
        ```js
        injectScript.injectString('function test(text) { console.log(text); }');
        ```
          
## injectStyle.js
* Inject stylesheets to current page.
* To add a stylesheet file to the head element use the following command:

     ```css
     injectStyle('http//to.your/stylesheet.css');
     ```
* Example:
     ```css
     injectStyle('https://cdnjs.cloudflare.com/ajax/libs/10up-sanitize.css/4.1.0/sanitize.min.css');
     ```
* This script also contains a list of available stylesheets.
   * Use "injectStyle.available_StyleSheets" to see a list of available scripts.
   * An example of a stylesheet that can be used from this usercript:
   
        ```js
        injectStyle.available_StyleSheets.sanitize.stylesheet
        ```
   * To get the link to a script's creator use the folling:
       
        ```js
        injectStyle.available_StyleSheets.sanitize.page
        ```
   * Available scripts have an inject function to inject the stylesheet.
     Here's an example:
         
        ```js
        injectStyle.available_StyleSheets.sanitize.inject();
        ```
   * Another way to inject available stylesheets is as follows:
       
        ```js
        injectStyle(injectStyle.available_StyleSheets.sanitize);
        ```
* CSS can also be added. Here's an example:
    ```css
    var css = `
        .testClass {
            color: red;
            background-color: black;
            border: 1px solid red;
        }
        .testClass2 {
            color: black;
            background-color: red;
            border: 1px solid balck;
        }
    `
    injectStyle.cssTextNode(css);
    ```
    * The variable above will be added as a style element to the head just like the following:
    
        ```html
        <style type="text/css" class="newCSSNode">
            .testClass {
               color: red;
               background-color: black;
               border: 1px solid red;
            }
            .testClass2 {
               color: black;
               background-color: red;
               border: 1px solid balck;
            }
        </style>
        ```

## rawgit_redirect.js
* Automatically redirects raw scripts in github to rawgit.com and gitlab.com to raw.githack.com.
* Script from rawgit can be loaded in other html pages.
* Content Security Policy needs to be set to "sandbox 'allow-scripts'" in order for this script to run on raw.githubusercontent.com. Look in Chrome WebStore for extensions to change this setting.

## bingVideosToNewTab.js
* Removes the emebeded video from and adds href with target \_blank to open the direct link in new tab.
* This also works from standard search queries as well as videos.

## googleClickTracking.js
* Removes mouse click tracking for seached links and opens links in a new tab.

## googlevideo.grid.js
* Uses gridstack.js to create a grid view of videos.

## superStackOverEx.js
* Open StackExchange\StackOverflow\Other links in a new tab.
* Changes links in comments\answers to red and inserts a red unicode checkbox before each link.

## wikiDisplay.js
* Changes fonts, font sizes, div width, deletes edit and references links, and changes hrefs from blue to black.

## TopRight-Menu.2.00.js
* Adds a dropdown menu to the top right corner of every page. The menu contains links to other scripts and pages I've been working on.

## realm.js
* realm is not a userscript, but could be added to deal with adding styles to elements easily.

* Add a single style.
    ```js
    var elm = realm('.classname');
    elm.css('color', 'red');
   
    //Or
    realm('.classname').css('color', 'red');
    ```
* Add multiple styles.
    ```js
    var elm = realm('.classname');
    elm.css({'width':'100%','height':'90px','background-color':'#ffffff'});
   
    //Or
    realm('.classname').css({'width':'100%','height':'90px','background-color':'#ffffff'});
    ```
* Add class name.
    ```js
    var elm = realm('.classname');
    elm.addClass('newclass');
   
    //Or
    realm('.classname').addClass('newclass');
    ```
* Get class list
    ```js
    var elm = realm('.classname');
    elm.getClassList;
   
    //Or
    realm('.classname').getClassList;
    ```
* Add stylesheet to head
    ```js
    var css = `.page {
       background-color: white;
       box-shadow: 0 10px 10px -10px grey;
       font: 100 22px/1.7 'Roboto', Tahoma, sans-serif;
       border-bottom: 1px solid black;
    }`
    realm.addStyleSheet(css);
    ```
