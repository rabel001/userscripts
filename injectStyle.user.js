// ==UserScript==
// @name            Inject StyleSheet
// @author          rabel001
// @homepage        https://rabel001.gitlab.io
// @version         2.1.14.19
// @description     Add stylesheets to current page using injectStylet("https://path/to/stylesheet.css");
// @description     Or injectStyle.available_StyleSheets.<stylesheet_name>.inject();
// @description     Or injectStyle(injectStyle.available_StyleSheets.<stylesheet_name>);
// @description     Or injectStyle.cssTextNode(".className { color: red; background: black; }");
// @namespace       https://gitlab.com/rabel001/userscripts/blob/master/injectStyle.user.js
// @updateURL       https://glcdn.githack.com/rabel001/userscripts/raw/master/injectStyle.user.js
// @downloadURL     https://glcdn.githack.com/rabel001/userscripts/raw/master/injectStyle.user.js
// @include         *
// @noframes
// @grant           none
// ==/UserScript==

;((global, doc) => {
     "use strict";

     let _injectStyle = (stylesheet) => {
          var help;
          if(typeof stylesheet == "object") {
               if(!!stylesheet.stylesheet) {
                    help = stylesheet.help;
                    stylesheet = stylesheet.stylesheet;
               }
          } else if(!(/\/\//.test(stylesheet))) {
               console.log("injectStyle Output:\n\t - Error: StyleSheet must be a url containing http/https.");
               return;
          }
          let promise = async function(stylesheet) {
               return new Promise((resolve, reject) => {
                    let continuation = (stylesheet) => {
                         let newStyleSheet = doc.createElement("link");
                         newStyleSheet.rel = "stylesheet";
                         newStyleSheet.href = stylesheet;
                         newStyleSheet.className = "newStyleSheet";
                         newStyleSheet.onerror = (event) => {
                              reject(`\n\t- Error:\n\t\t- StyleSheet loaded: false\n\t\t- Error event type: ${event.type}\n\t\t- StyleSheet: ${stylesheet}`);
                         };
                         newStyleSheet.onload = (event) => {
                              resolve(`\n\t- StyleSheet loaded: true\n\t- StyleSheet element: ${newStyleSheet.outerHTML}`);
                         };
                         doc.head.appendChild(newStyleSheet);
                    };
                    if(!(/\.css$/i.test(stylesheet))) {
                         reject(`\n\t- Error:\n\t\t- File name: ${stylesheet}.\n\t\t- ERROR: File extension should by ".css".`);
                    } else if(doc.getElementsByClassName("newStyleSheet").length !== 0) {
                         let sheets = Array.from(doc.getElementsByClassName("newStyleSheet"));
                         let arr = [];
                         for(let i = 0; i < sheets.length; i++) {
                              let sheet = sheets[i];
                              arr.push(sheet.href);
                         }
                         if(!!~arr.indexOf(stylesheet)) {
                              reject("\n\t- Error: StyleSheet already loaded");
                         } else {
                              continuation(stylesheet);
                         }
                    } else {
                         continuation(stylesheet);
                    }
               });
          };
          promise(stylesheet).then((data) => {
               console.log("injectStyle Output:", data);
               if(help !== undefined) {
                    console.log("\t- StyleSheet help:", help);
               }
          }).catch((error) => {
               console.log("injectStyle Output:", error);
          });
     };
     _injectStyle.cssTextNode = (str) => {
          let promise = async function(nstr) {
               return new Promise((resolve, reject) => {
                    let style = doc.createElement("style");
                    style.type = "text/css";
                    let cssTextNode = doc.createTextNode(nstr);
                    style.appendChild(cssTextNode);
                    style.className = "newCSSNode";
                    style.onerror = (event) => {
                         reject(`\n\t- Error:\n\t\t- CSS loaded: false\n\t\t- Error event type: ${event.type}`);
                    };
                    style.onload = (event) => {
                         resolve(`\n\t- CSS loaded: true\n\t- Style element:\n ${style.outerHTML}`);
                    };
                    doc.head.appendChild(style);
               });
          };
          promise(str).then((data) => {
               console.log("injectStyle Output:", data);
          }).catch((error) => {
               console.log("injectStyle Output:", error);
          });
     };
     _injectStyle.available_StyleSheets = {
          animate: {
               stylesheet: "//rawgit.com/daneden/animate.css/master/animate.css",
               help: {
                    page: "http://daneden.github.io/animate.css"
               }
          },
          sanitize: {
               stylesheet: "//cdnjs.cloudflare.com/ajax/libs/10up-sanitize.css/4.1.0/sanitize.min.css",
               help: {
                    page: "https://jonathantneal.github.io/sanitize.css"
               }
          },
          reset: {
               stylesheet: "//cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css",
               help: {
                    page: "http://meyerweb.com/eric/tools/css/reset/"
               }
          },
          normalize: {
               stylesheet: "//necolas.github.io/normalize.css/8.0.0/normalize.css",
               help: {
                    page: "https://necolas.github.io/normalize.css"
               }
          },
          dataTables: {
               stylesheet: "//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css",
               help: {
                    page: "https://www.datatables.net"
               }
          }
     };
     for(let key in _injectStyle.available_StyleSheets) {
          if(_injectStyle.available_StyleSheets.hasOwnProperty(key)) {
               _injectStyle.available_StyleSheets[key].inject = function() {
                    _injectStyle(this);
               };
               _injectStyle.available_StyleSheets[key].help.open = function() {
                    window.open(this.page, "_blank");
               };
          }
     }
     global.injectStyle = _injectStyle;
})(window, document);