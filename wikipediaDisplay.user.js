// ==UserScript==
// @name            Wikipedia Display
// @author          rabel001
// @homepage        https://rabel001.gitlab.io
// @version         2.1.14.19
// @icon            https://en.wikipedia.org/favicon.ico
// @description     Change fonts, font sizes, change hrefs to black, delete edit and reference links.
// @namespace       https://gitlab.com/rabel001/userscripts/blob/master/wikipediaDisplay.user.js
// @updateURL       https://glcdn.githack.com/rabel001/userscripts/raw/master/wikipediaDisplay.user.js
// @downloadURL     https://glcdn.githack.com/rabel001/userscripts/raw/master/wikipediaDisplay.user.js
// @include         *wikipedia.org/wiki*
// @include         *rightpedia*
// @noframes
// @grant           none
// ==/UserScript==

;((global) => {
     "use strict";

     global.changeDisplay = () => {
          var all = Array.from;
          Object.assign(all, {
               prototype: Array.prototype
          });
          Object.assign(all.prototype, {
               is: Array.prototype.map
          });
          all(document.querySelectorAll("p")).is((e) => console.log(e));
          all(document.querySelectorAll("a")).is((e) => {
               e.style.color = "black";
          });
          document.querySelectorAll("sup")[0].remove();
          document.querySelector("#toc").remove();
          document.querySelector("#siteSub").remove();
          document.querySelector("#content").style.width = "60%";
          all(document.querySelectorAll("p")).is((e) => {
               e.style.lineHeight = "1.2em";
          });
          all(document.querySelectorAll(".mw-body-content")).is((e) => {
               e.style.fontSize = "1.2em";
          });
          all(document.querySelectorAll(".mw-editsection")).is((e) => e.remove());
          all(document.querySelectorAll(".reference")).is((e) => e.remove());
          document.querySelector("#content").style.width = "55%";
          document.body.style.fontFamily = "Lora,serif";
     };


     var bodyContent = document.getElementById('bodyContent');
     bodyContent.querySelectorAll("a").forEach((link) => {
          link.target = "_blank";
          link.style.color = "red";
     });
})(window);